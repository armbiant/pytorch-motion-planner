import numpy as np
import torch
import torch.nn as nn
from torch.nn import functional
import typing as tp
import string
from threading import Lock
from .continuous_planner import ContinuousPlanner
from .utils.timer import timer

from neural_field_optimal_planner.collision_checker import CollisionChecker
from .onf_model import ONF
from torch import autograd
from .utils.position2 import Position2
from copy import deepcopy

class TimedPlanner(ContinuousPlanner):

    def __init__(self,
                 trajectory: torch.Tensor,
                 collision_model: ONF,
                 collision_checker: CollisionChecker,
                 collision_optimizer: torch.optim.Optimizer,
                 trajectory_optimizer: torch.optim.Optimizer,
                 trajectory_random_offset: float,
                 collision_weight: float,
                 velocity_hessian_weight: float,
                 init_collision_iteration: int = 100,
                 init_collision_points: int = 100,
                 reparametrize_trajectory_freq: int = 10,
                 optimize_collision_model_freq: int = 1,
                 random_field_points: int = 10,
                 collision_loss_koef: float = 1,
                 course_random_offset: float = 1.5,
                 collision_point_count: int = 100,
                 device: str = "cpu") -> None:
        """NERF Optimal Motion PLanner.
        The paper https://ieeexplore.ieee.org/document/9851532 

        Args:
            - `trajectory (torch.zeros)`: Matrix of size `m * n`, m - point `[x,y,theta]`, `n` - 
                points amount. `requires_grad` stands for gradient computation, must be `True`.

            - `collision_model (ONF)`: Optimal Neural Field class model, derived from torch.nn.Module, 
                base class for all NNs.

            - `collision_checker (CollisionChecker)`: CollisionChecker class (custom class). 
                Ref to `CollisionChecker`.

            - `collision_optimizer (torch.optim)`: `torch.Optim` optimizer.

            - `trajectory_optimizer (torch.optim)`: `torch.Optim` optimizer.

            - `trajectory_random_offset (float)`: Sigma of random points that spawn along the trajectory.
                 Ref value is `0.02`.

            - `collision_weight (float)`: hyperparameter of ONF, handles the weight of collision loss in
                loss sum function. Ref value is `1`.

            - `velocity_hessian_weight (float)`: parameter of path planner, responsible for trajectory 
                U-shape and thin obstacles handling. Ref value is `0.5`.

            - `init_collision_iteration (int, optional)`: Amount of iterations to initialize collision model.
                 Defaults to `100`.

            - `init_collision_points (int, optional)`: Amount of points which are randomly sampled in each 
                `collision_model` initialization iteration. Defaults to `100`.

            - `reparametrize_trajectory_freq (int, optional)`: Frequency of trajectory reparametrization 
                in [steps], e.g. each `i`-th step trajectory will be reparametrized. Reparametrization is
                spacing the points of the trajectory along it's "center line" evenly (sort of linspace),
                in order to handle U-shaped and thin obstacles. Defaults to `10`.

            - `optimize_collision_model_freq (int, optional)`: Frequency of collision model optimization 
                in [steps], e.g. each `i`-th step collision model will be updated. Defaults to `1`.

            - `random_field_points (int, optional)`: Amount of points which are randomly sampled in each 
                `collision_model` iteration across all the field. Defaults to `10`.

            - `course_random_offset (float, optional)`: Sigma of random points that spawn along the trajectory.
                 Ref value is `0.02`. Defaults to `1.5`.

            - `collision_point_count (int, optional)`:  Amount of points which are randomly sampled in each 
                `collision_model` iteration near the area of high collision loss and along the trajectory.
                 Defaults to `100`.

            - device (string, optional): the device planner will run

        Returns:
            None
        """

        torch.autograd.set_detect_anomaly(True)
        self._trajectory: torch.Tensor = trajectory
        self._goal_point: torch.Tensor = torch.zeros(1, 2, device=device)
        self._start_point: torch.Tensor = torch.zeros(1, 2, device=device)
        self._device: str = device
        self._collision_model: ONF = collision_model
        self._collision_checker: CollisionChecker = collision_checker
        self._collision_optimizer: torch.optim.Optimizer = collision_optimizer
        self._trajectory_optimizer: torch.optim.Optimizer = trajectory_optimizer
        self._collision_loss_function: torch.nn.Module = nn.BCEWithLogitsLoss()
        self._random_sample_border: np.ndarray = np.array([0, 0, 0, 0])
        self._fine_random_offset: float = trajectory_random_offset
        self._collision_weight: float = collision_weight
        self._inv_hessian: torch.Tensor = self._calculate_inv_hessian(
            self._trajectory.shape[0], velocity_hessian_weight)
        self._init_collision_iteration: int = init_collision_iteration
        self._init_collision_points: int = init_collision_points
        self._reparametrize_trajectory_freq: int = reparametrize_trajectory_freq
        self._optimize_collision_model_freq: int = optimize_collision_model_freq
        self._random_field_points: int = random_field_points
        self._step_count: int = 0
        self._collision_loss_koef: float = collision_loss_koef
        self._previous_trajectory: tp.Optional[torch.Tensor] = None
        self._collision_positions: np.ndarray = np.zeros((0, 3))
        self._collision_positions_times: np.ndarray = np.zeros(0)
        self._course_random_offset: float = course_random_offset
        self._collision_point_count: int = collision_point_count
        self.checked_positions: Position2 = Position2.from_vec(
            np.zeros((0, 3)))
        self.truth_collision: np.ndarray = np.zeros(0, dtype=np.bool8)
        self.obstacles_trajectories: dict = dict()

    def update_obstacles_trajectories(self, trajectories: dict):
        for traj in trajectories:
            assert trajectories[traj].shape[1] == 4, "Bad trajectory shape in update obstacles trajectories"
            self.obstacles_trajectories[traj] = trajectories[traj]    

    def _calculate_inv_hessian(self,
                               point_count: int,
                               velocity_hessian_weight: float) -> torch.Tensor:
        """ Calculate inverse Hessian matrix for gradient pre-conditioning

        Args:
            - `point_count` (int): size of the matrix (trajectory size)
            - `velocity_hessian_weight` (float): hyperparameter, ref. value is 0.5

        Returns:
            - `inv_hessian` (torch.tensor): Inverted precalculated Hessian matrix
        """
        hessian: np.ndarray = velocity_hessian_weight * \
            self._calculate_velocity_hessian(point_count) + np.eye(point_count)
        inv_hessian: np.ndarray = np.linalg.inv(hessian)
        return torch.tensor(inv_hessian.astype(np.float32), device=self._device)

    def _calculate_velocity_hessian(self, point_count: int) -> np.ndarray:
        """ Calculate Hessian matrix

        Args:
            - `point_count` (int): size of the matrix (trajectory size)

        Returns:
            - `hessian` (np.ndarray): Hessian matrix
        """
        hessian: np.ndarray = np.zeros(
            (point_count, point_count), dtype=np.float32)
        for i in range(point_count):
            hessian[i, i] = 4
            if i > 0:
                hessian[i, i - 1] = -2
                hessian[i - 1, i] = -2
        return hessian

    def step(self) -> None:
        """ Function performs one step of planner (collision and trajectory) optimization

        Description:
            - It performs one step of optimization (training) of collision model each
                `i`-th step of function call, where the `i` is `_optimize_collision_model_freq`
                in [counts], or [steps].
            - The same remains for optimization of the trajectory, but `i` here is
                `_reparametrize_trajectory_freq` in [counts], or [steps].

            The function pefromance may be simply benchmarked by the global Timer.timer object.
            Ref to the .utils.Timer.
        """
        timer.tick("step")
        # Every step do optimize as _optimize_collision_model_freq == 1
        if self._step_count % self._optimize_collision_model_freq == 0:
            self._optimize_collision_model()
        self._optimize_trajectory()

        timer.tick("reparametrize_trajectory")
        if self._step_count % self._reparametrize_trajectory_freq == 0:  # Every 10th step
            with torch.no_grad():
                self.reparametrize_trajectory()
        timer.tock("reparametrize_trajectory")
        self._step_count += 1
        timer.tock("step")

    def get_full_trajectory(self) -> torch.Tensor:
        """ Concatenate trajectory, starting point, and goal point

        Returns:
            - `torch.Tensor`: Full trajectory `m * n+2`, `m` - point `[x,y]`, `n` - points amount
        """
        trajectory = deepcopy(self._trajectory)
        start = deepcopy(self._start_point)
        goal = deepcopy(self._goal_point)
        return torch.cat([start, trajectory, goal], dim=0).detach().cpu().numpy()

    def _full_trajectory(self) -> torch.Tensor:
        """ Concatenate trajectory, starting point, and goal point

        Returns:
            - `torch.Tensor`: Full trajectory `m * n+2`, `m` - point `[x,y]`, `n` - points amount
        """
        # trajectory = deepcopy(self._trajectory)
        # start = deepcopy(self._start_point)
        # goal = deepcopy(self._goal_point)
        # return torch.cat([start, trajectory, goal], dim=0)
        return torch.cat([self._start_point, self._trajectory, self._goal_point], dim=0)


    def _optimize_collision_model(self, positions: tp.Optional[np.ndarray] = None) -> None:
        """ Call one step of collision model optimization e.g. one step of traininig of neural field

        Description:

            - Firstly, it samples collision points in a special way around the existing trajectory.
                See `sample_collision_checker_points()`. It is needed for collision
                optimization near the areas where collision probability is high.
            - Secondly, we enable gradient calculation for collision model tensor and reset
                gradient for optimizer. This is needed for indicating that `collision_model` is
                trainable and it's tensors's gradient should be computed. As for `collision_optimizer`,
                gradient is reset to zeros in order to train it once again after loss has been updated,
                e.g. optimize a new function.
            - At last, we train the collision model, using `truth_collision` as a target
                variable.
        """
        timer.tick("optimize_collision_model")
        if positions is None:
            if self._previous_trajectory is None:
                self._previous_trajectory = self._trajectory.detach().clone()

            positions = self._sample_collision_checker_points(
                self._previous_trajectory)

            self._previous_trajectory = self._trajectory.detach().clone()
        self._collision_model.requires_grad_(True)
        self._collision_optimizer.zero_grad()
        # Forward (predict)
        predicted_collision: np.ndarray = self._calculate_predicted_collision(
            positions)
        truth_collision: torch.Tensor = torch.tensor(self._calculate_truth_collision(positions).astype(np.float32)[:, None],
                                                     device=self._device)
        loss: torch.Tensor = self._collision_loss_function(
            predicted_collision, truth_collision)
        # Calculate gradients of Tensor
        loss.backward()
        # Optimize the nn by changing its parameters
        self._collision_optimizer.step()
        timer.tock("optimize_collision_model")

    def _calculate_truth_collision(self, positions: np.ndarray) -> np.ndarray:
        """Calculate truth collision

        Args:
            positions (np.ndarray): Points to check. Matrix of size `m * n`, m - point `[x,y]`, `n` - 
                points amount.
        Returns:
            np.ndarray: Matrix of size
        """
        #self.checked_positions = positions.copy()
        #self.truth_collision = self._collision_checker.check_collision(positions)
        # return self.truth_collision

        dim = positions.shape[1] # костыль
        if (dim == 2):
            self.checked_positions = Position2.from_vec(np.concatenate(
                (positions, np.zeros((positions.shape[0], 1))), axis=1).copy())
        else:
            self.checked_positions = Position2.from_vec(positions)

        self.truth_collision = self._collision_checker.check_collision(
            self.checked_positions)
        return self.truth_collision

    def _calculate_predicted_collision(self, positions: np.ndarray) -> np.ndarray:
        """Get collision probabilities from the collision model (ONF)

        Args:
            positions (np.ndarray): Positions [x, y, theta] to check
        Returns:
            np.ndarray: Array (column) of collision probabilities
        """
        return self._collision_model(torch.tensor(positions.astype(np.float32), device=self._device))

    def _sample_collision_checker_points(self, trajectory: torch.Tensor) -> np.ndarray:
        """Generate new set of points, which are to be checked by collision model
            and by true collision checker at next step

        Args:
            trajectory (torch.Tensor): set of points, which is used to sample new points around,
                namely near trajectory itself, near the high collision loss areas, and across the
                field

        Returns:
            np.ndarray: new points, the length is different from input trajectory, but the points
                remain the same dim (for example, `[x,y,theta]`)
        """
        positions: np.ndarray = self._random_intermediate_positions(trajectory).detach().cpu().numpy()
        course_positions: np.ndarray = self._offset_positions(positions, self._course_random_offset)
        fine_positions: np.ndarray = self._offset_positions(positions, self._fine_random_offset)
        times: np.ndarray = np.concatenate([self._collision_positions_times, np.zeros(len(fine_positions))], axis=0)
        positions = np.concatenate([self._collision_positions, fine_positions], axis=0)
        self._collision_positions, self._collision_positions_times = self._resample_collision_positions(positions, times)
        return np.concatenate([course_positions, self._collision_positions, self._sample_random_field_points(self._random_field_points)], axis=0)

    def _random_intermediate_positions(self, trajectory: torch.Tensor) -> torch.Tensor:
        """ Generate random positions between each `i`-th and `i+1` points of the trajectory
        
        Args:
            trajectory (torch.Tensor): Matrix of size `m * n`, m - point `[x,y,theta]`, `n` - 
                points amount.
        Returns:
            torch.Tensor: Matrix of the same size with resampled points
        """
        if trajectory is None:
            trajectory = self._trajectory
        if trajectory.size == 0:
            trajectory = self._trajectory    
        t = torch.tensor(np.random.rand(trajectory.shape[0] - 1).astype(np.float32), device=self._device)[:, None]
        return trajectory[1:] * (1 - t) + trajectory[:-1] * t

    def _offset_positions(self, positions: np.ndarray, offset: float) -> np.ndarray:
        """Generate random offsets for each point of the trajectory e.g. bias each point of the trajectory

        Args:
            positions (np.ndarray): Matrix of size `m * n`, m - point `[x,y,theta]`, `n` - 
                points amount.
            offset (float): Amount of bias to the each point (multiplier of randn[0...1])

        Returns:
            np.ndarray: Matrix of the same size with biased points
        """
        dim = positions.shape[1]
        return positions + np.random.randn(positions.shape[0], dim) * offset

    def _resample_collision_positions(self, positions, times):
        """Resample points in area with high collision probability.
        Resample points which are "too long" in the collision.
        Args:
            positions (_type_): 
            times (_type_): 

        Returns:
            _type_: 
        """
        with torch.no_grad():
            predicted_collision = self._calculate_predicted_collision(positions)
            weights = torch.sigmoid(predicted_collision).detach().cpu().numpy()[:, 0]
            weights = weights * np.exp(-times * 0.03) + 1e-6
            weights = weights / np.sum(weights)
        if len(positions) < self._collision_point_count:
            return positions, times
        replace = np.count_nonzero(weights > 1e-6) < self._collision_point_count
        indices = np.random.choice(len(positions), self._collision_point_count, replace=replace, p=weights)
        times = times + 1
        return positions[indices], times[indices]

    def _sample_random_field_points(self, points_count: int) -> np.ndarray:
        """Generate random points in the given field boundaries

        Args:
            points_count (int): Amount of points

        Returns:
            np.ndarray: Sampled points `m * n`, `m = points_count`, `n` - point dimension ([x,y,theta])
        """
        dim = self._collision_positions.shape[1] # костыль

        random_points = np.random.rand(points_count, dim)
        random_points[:, 0] = self._random_sample_border[0] + random_points[:, 0] * (self._random_sample_border[1] - self._random_sample_border[0])
        random_points[:, 1] = self._random_sample_border[2] + random_points[:, 1] * (self._random_sample_border[3] - self._random_sample_border[2])
        return random_points

    def _optimize_trajectory(self):
        """Optimize trajectory using Gauss-Newton method
           https://en.wikipedia.org/wiki/Gauss%E2%80%93Newton_algorithm

           Ref to `_optimize_collision_model` for more info
        """
        self._collision_model.requires_grad_(False)
        self._trajectory_optimizer.zero_grad()
        loss = self.trajectory_loss()
        timer.tick("trajectory_backward")
        loss.backward()
        timer.tock("trajectory_backward")
        timer.tick("inv_hes_grad_multiplication")
        self._trajectory.grad = self._inv_hessian @ self._trajectory.grad
        timer.tock("inv_hes_grad_multiplication")
        timer.tick("trajectory_optimizer_step")
        self._trajectory_optimizer.step()
        timer.tock("trajectory_optimizer_step")

    def trajectory_loss(self) -> torch.Tensor:
        """Calculate summary loss for the promoted trajectory
            
            According to the paper: L traj = L distance + w col * L col
                (unconstrained planner)

        Returns:
            torch.Tensor: Loss sum  (L trajectory)
        """
        # collision_positions = self._random_intermediate_positions()
        collision_positions: torch.Tensor = self._random_intermediate_positions(self._trajectory)
        return self.distance_loss() + self.trajectory_collision_loss(collision_positions) * self._collision_weight

    def distance_loss(self) -> torch.Tensor:
        """Calculate distance loss from the existing trajectory
            Goal is to have the shortest trajectory

        Returns:
            torch.Tensor: Distance loss (L distance)
        """
        full_trajectory = self._full_trajectory()
        delta = full_trajectory[1:] - full_trajectory[:-1]
        return torch.sum(delta ** 2)

    def trajectory_collision_loss(self, collision_positions: torch.Tensor) -> torch.Tensor:
        """Calculate trajectory collision loss from the collision model
            Goal is to have zero collisions and be as far from obstacles as possible.
        
        Args:
            collision_positions (torch.Tensor): points to check collision probability for

        Returns:
            torch.Tensor: Collision loss (L collision)
        """
        collision_probabilities = self._collision_model(collision_positions)
        collision_probabilities = nn.functional.softplus(collision_probabilities)
        return torch.sum(collision_probabilities)

    def boundary_loss(self):
        loss = torch.relu(-self._trajectory[:, 0] +
                          self._random_sample_border[0]) ** 2
        loss.add_(torch.relu(
            self._trajectory[:, 0] - self._random_sample_border[1]) ** 2)
        loss.add_(
            torch.relu(-self._trajectory[:, 1] + self._random_sample_border[2]) ** 2)
        loss.add_(torch.relu(
            self._trajectory[:, 1] - self._random_sample_border[3]) ** 2)
        return torch.sum(loss)

    def get_path(self) -> np.ndarray:
        """Returns the full trajectory of planner

        Returns:
            np.ndarray: Full trajectory
        """
        return self._full_trajectory().detach().cpu().numpy()

    def init(self, start_point: np.ndarray, goal_point: np.ndarray, boundaries: np.ndarray):
        """Initialize planner with desired points and boundaries

        Args:
            start_point (np.ndarray): [x, y, theta] [m], [m], [rad]
            goal_point (np.ndarray): [x, y, theta] [m], [m], [rad]
            boundaries (np.ndarray): [-x, +x, -y, +y] [m], [m], [m], [m]
        """
        self._start_point = torch.tensor(start_point.astype(np.float32), device=self._device)[None]
        self._goal_point = torch.tensor(goal_point.astype(np.float32), device=self._device)[None]
        self._random_sample_border = boundaries
        self._init_trajectory()
        self._init_collision_model()
        self._step_count = 0

    def _init_trajectory(self):
        """Initialize trajectory with a straight line from start to end
        """
        with torch.no_grad():
            trajectory_length = self._trajectory.shape[0] + 2
            self._trajectory[:, 0] = torch.linspace(self._start_point[0, 0], self._goal_point[0, 0], trajectory_length)[1:-1]
            self._trajectory[:, 1] = torch.linspace(self._start_point[0, 1], self._goal_point[0, 1], trajectory_length)[1:-1]

    def _init_collision_model(self):
        """Sample initial set of points for the model training
        """
        for i in range(self._init_collision_iteration):
            positions = self._sample_random_field_points(self._init_collision_points)
            self._optimize_collision_model(positions)

    def update_goal_point(self, goal_point):
        self._goal_point = torch.tensor(goal_point.astype(np.float32), device=self._device)[None]
        with torch.no_grad():
            delta = torch.sum((self._trajectory - self._goal_point) ** 2, dim=1)
            min_index = torch.argmin(delta)
            self._trajectory.data[min_index:] = self._goal_point
            self.reparametrize_trajectory()
        self._step_count = 0

    def update_start_point(self, start_point):
        self._start_point = torch.tensor(start_point.astype(np.float32), device=self._device)[None]
        with torch.no_grad():
            delta = torch.sum((self._trajectory - self._start_point) ** 2, dim=1)
            min_index = torch.argmin(delta)
            self._trajectory.data[:min_index] = self._start_point
            self.reparametrize_trajectory()
        self._step_count = 0

    def set_boundaries(self, boundaries: np.ndarray):
        """Update boundaries and reset planner

        Args:
            boundaries (np.ndarray): [-x, +x, -y, +y] [m], [m], [m], [m]
        """
        self._random_sample_border = boundaries
        self._step_count = 0

    def reparametrize_trajectory(self) -> None:
        full_trajectory = self._full_trajectory()
        distances = self._calculate_distances()
        normalized_distances = distances / torch.sum(distances)
        cdf = torch.cumsum(normalized_distances, dim=0)
        cdf = torch.cat([torch.zeros(1, device=self._device), cdf], dim=0)
        uniform_samples = torch.linspace(0, 1, len(full_trajectory), device=self._device)[1:-1]
        indices = torch.searchsorted(cdf, uniform_samples)
        index_above = torch.where(indices > len(
            full_trajectory) - 1, len(full_trajectory) - 1, indices)
        index_bellow = torch.where(indices - 1 < 0, 0, indices - 1)
        cdf_above = torch.gather(cdf, 0, index_above)
        cdf_bellow = torch.gather(cdf, 0, index_bellow)
        index_above = torch.repeat_interleave(
            index_above[:, None], self._trajectory.shape[1], dim=1)
        index_bellow = torch.repeat_interleave(
            index_bellow[:, None], self._trajectory.shape[1], dim=1)
        trajectory_above = torch.gather(full_trajectory, 0, index_above)
        trajectory_bellow = torch.gather(full_trajectory, 0, index_bellow)
        denominator = cdf_above - cdf_bellow
        denominator = torch.where(
            denominator < 1e-5, torch.ones_like(denominator) * 1e-5, denominator)
        t = (uniform_samples - cdf_bellow) / denominator
        trajectory = (1 - t[:, None]) * trajectory_bellow + \
            t[:, None] * trajectory_above
        self._trajectory.data = trajectory

    def _calculate_distances(self) -> torch.Tensor:
        """ Calculate norm from every `i` to `i+1` point

        Returns:
            torch.Tensor: Sampled points `m * n`, `m = points_count`, `n` - point dimension ([x,y,theta])
        """
        full_trajectory = self._full_trajectory()
        return torch.norm(full_trajectory[1:] - full_trajectory[:-1], dim=1)
