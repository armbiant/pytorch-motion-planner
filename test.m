close all
%dir = [-0.7967506051063538, -0.6043077707290649, -0.0008032527402974665];
%up = [0.00481778709217906, -0.00725077698007226, 0.9999620914459229];

dir = [1, 0, 0];
up = [0, 0, 1];
left = cross(up, dir);

plot3([0 dir(1)], [0 dir(2)], [0 dir(3)], color='red', LineWidth=2);
hold on
grid on
axis equal
plot3([0 up(1)], [0 up(2)], [0 up(3)], color='blue', LineWidth=2);
plot3([0 left(1)], [0 left(2)], [0 left(3)], color='green', LineWidth=2);

plot3([0 1], [0 0], [0 0], color='red');
plot3([0 0], [0 1], [0 0], color='green');
plot3([0 0], [0 0], [0 1], color='blue');

tx = -128*(pi/180);
ty = 180*(pi/180);
tz = 0*(pi/180);
Rx = [1 0 0; 0 cos(tx) -sin(tx); 0 sin(tx) cos(tx)];
Ry = [cos(ty) 0 sin(ty); 0 1 0; -sin(ty) 0 cos(ty)];
Rz = [cos(tz) -sin(tz) 0; sin(tz) cos(tz) 0; 0 0 1];

M = Rz*Ry*Rx*[dir; left; up];
q = rotm2quat(M);
disp(strcat(compose("%9.2f", q(1)), ",", compose("%9.2f", q(2)), ...
    ",", compose("%9.2f", q(3)), ",", compose("%9.2f", q(4))))
