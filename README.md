# Pytorch motion planner
## Description
This repository contains code for Neural field optimal path planner (NFOPP)

The idea is to construct Optimal path planner with Laplacian regularization. 
For optimization of absence of obstacles, it is possible to use 
Neural field which predict probability of collision for given position. 
Thus, loss function consists of three part: collision loss function
which minimize position in collision, Laplacian regularization which 
minimize path length and total rotation, and allow optimizing smooth
trajectory, and constraint loss which forces position to specific constraint. 
Constraint loss can be done with Lagrangian multipliers

### Planner is capable

- Make optimal point to point paths which combine rotation and translation loss
- Make non-holonomic optimal paths
- Make non-holonomic with only forward movement paths
- Make paths in corridor with obstacles
- Make paths in corridor with obstacles with non-holonomic only forward movement
- Make paths in environment with random obstacles 

### Main features

- Laplacian's regularization for points on path as distance loss term (CHOMP loss term)
- Neural field representation for collision loss which can generate smooth gradients
- Continuous simultaneous learning for collision neural field with path points
- Lagrangian's multipliers as loss term for learning strict and not-strict constraints
- Quadratic loss term for constraints for fast initial convergence (with Lagrangian multipliers)
- Trajectory re-parametrization

# Prerequisites
**Python 3.9**

**Python modules**
- pytorch 1.9.0+cu111
- numpy
- matplotlib
- PyYAML

before installing torch, ensure that you install it for the right driver https://pytorch.org/get-started/locally/

Check CUDA version
```bash
nvidia-smi
```
it should print sth like
```bash
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 520.56.06    Driver Version: 520.56.06    CUDA Version: 11.8     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  NVIDIA GeForce ...  Off  | 00000000:01:00.0  On |                  N/A |
| 37%   34C    P8    14W / 220W |   1668MiB /  8192MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|    0   N/A  N/A      1001      G   /usr/lib/xorg/Xorg                102MiB |
|    0   N/A  N/A      1539      G   /usr/lib/xorg/Xorg                578MiB |
|    0   N/A  N/A      1669      G   /usr/bin/gnome-shell              239MiB |
|    0   N/A  N/A      4781      G   telegram-desktop                    6MiB |
|    0   N/A  N/A      8374      G   ...RendererForSitePerProcess      121MiB |
|    0   N/A  N/A      8378      G   ...674891252934105170,131072      605MiB |
+-----------------------------------------------------------------------------+
```
then select the corresponding version from the site.

```bash
pip3 install torch torchvision torchaudio --extra-index-url #put the corresponding index here
pip3 install pytorch-lightning
pip3 install numpy matplotlib pyyaml scipy
```

**ROS installation**
- [ROS Noetic](http://wiki.ros.org/noetic/Installation)
- hermesbot_collision_checker
- hermesbot_navigation
- hermesbot_simulation

```bash
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt install curl # if you haven't already installed curl
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
sudo apt install ros-noetic-desktop-full
```
**Benchmark** 

- bench-mr
   - OMPL 1.5+
   - SBPL 1.3.1+
   - nlohmann-json
   - libccd 1.4+ 
   - python3
- libpython-dev (for python binding)
- python-dev (for python binding)
- python3-tk (for visualization)

```bash
git clone https://github.com/nlohmann/json.git
cd json
mkdir build
cd build
cmake ..
make -j8
sudo make install
```

```bash
git clone https://github.com/sbpl/sbpl.git
cd sbpl
mkdir build
cd build
cmake ..
make -j8
sudo make install
```

```bash
git clone https://github.com/ompl/ompl.git
```
then download [OMPL installation script](https://ompl.kavrakilab.org/core/install-ompl-ubuntu.sh)
```bash
chmod u+x install-ompl-ubuntu.sh
./install-ompl-ubuntu.sh --python
```

Go to the repo folder, then load submodules (bench-mr, pybind and their submodules)

```bash
git submodule update --force --recursive --init --remote 
```

Build Bench mr

```bash
sudo bash benchmark/third_party/bench-mr/scripts/build.sh
```

Build bench mr python binding
```bash
sudo rm -rf build
mkdir build
cd build
sudo cmake -DINSTALL_BENCHMARK=ON ..
sudo make -j8
```

[!NOTE] After building, a new shared library "build/benchmark/pybench_mr.so" must 
be created



# Running

## Python module
```bash
sudo python3 setup.py install
```
To check installation run script
```bash
python3 scripts/run_planner.py
```

## Running Benchmark

For running experiments with benchmark see notebooks/benchmark folder

To run planner with benchmark environment, change the path to the robot shape in `2022-01-14_17-19-42_config.json`, line `77` to your path.
 
Then run following command
```bash
export PYTHONPATH=$PYTHONPATH:build/benchmark
python3 scripts/run_bench_mr.py test/test_benchmark/2022-01-14_17-19-42_config.json --show true
```
