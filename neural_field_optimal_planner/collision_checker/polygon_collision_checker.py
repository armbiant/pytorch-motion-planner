import numpy as np

from neural_field_optimal_planner.collision_checker import CollisionChecker
import numpy as np
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely.ops import unary_union
from shapely.validation import make_valid
import geopandas as gpd
from copy import copy
import time

class PolygonCollisionChecker():
    def __init__(self, box: list, polygon_map: gpd.GeoSeries, collision_boundaries: list):
        """
        Boundaries (-x, +x, -y, +y)
        Box (-x, +x, -y, +y)
        """
        self._polygon_map = polygon_map
        self._boundaries = collision_boundaries
        self._box = gpd.GeoSeries([
                Polygon([(box[0], box[1]), (box[1], box[2]), (box[2], box[3]), (box[3], box[0])])
        ])
        self._box.plot()
        time.sleep(5)

    def _check_boundaries_collision(self, test_positions):
        if self._boundaries is None:
            return False
        result = test_positions[:, 0] > self._boundaries[1]
        result |= test_positions[:, 0] < self._boundaries[0]
        result |= test_positions[:, 1] > self._boundaries[3]
        result |= test_positions[:, 1] < self._boundaries[2]
        return result

    def update_map(self, points: gpd.GeoSeries):
        self._obstacle_points = points

    def update_boundaries(self, boundaries: list):
        self._boundaries = boundaries

    def get_boundaries(self):
        return self._boundaries

    def check_collision(self, positions):
        res = np.zeros((len(positions.translation),), dtype=np.bool_)
        for i in range(len(positions.translation)):
            box = copy(self._box)
            box = box.rotate(positions.rotation[i], origin="center", use_radians=True)
            box = box.translate(positions.translation[i][0], positions.translation[i][1], 0)
            # if need to get coordinates
            # print([list(box.geometry.exterior[row_id].coords) for row_id in range(box.shape[0])])
            res[i] = ~(self._polygon_map.contains(box).any())
        bounds = np.array([not elem for elem in self._check_boundaries_collision(positions.translation)], dtype=np.bool_) 
        return res
