from threading import Thread
from neural_field_optimal_planner.test_environment_builder import TestEnvironmentBuilder
from neural_field_optimal_planner.plotter import *
from neural_field_optimal_planner.plotting_utils import *
from neural_field_optimal_planner.planner_factory import PlannerFactory
from neural_field_optimal_planner.collision_checker import CircleDirectedCollisionChecker, RectangleCollisionChecker, CarCollisionChecker
import time
from pytorch_lightning.utilities import AttributeDict
import torch
import numpy as np
import sys
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner')


torch.random.manual_seed(100)
np.random.seed(400)

planner_parameters = AttributeDict(
    device="cpu",
    trajectory_length=20,
    collision_model=AttributeDict(
        mean=1,
        sigma=5,
        use_cos=True,
        bias=True,
        use_normal_init=True,
        angle_encoding=True,
        name="ONF"
    ),
    trajectory_initializer=AttributeDict(
        name="TrajectoryInitializer",
        resolution=0.05
    ),
    collision_optimizer=AttributeDict(
        lr=1e-2,
        betas=(0.75, 0.75)
    ),
    trajectory_optimizer=AttributeDict(
        lr=2e-1,
        betas=(0.99, 0.99)
    ),
    planner=AttributeDict(
        name="ConstrainedNERFOptPlanner",
        velocity_hessian_weight=4,
        angle_weight=6,
        collision_weight=20,
        constraint_deltas_weight=20,
        boundary_weight=2,
        distance_loss_weight=230,

        trajectory_random_offset=0.1,
        angle_offset=0.3,
        random_field_points=10,
        init_collision_iteration=10,
        init_collision_points=100,
        reparametrize_trajectory_freq=1,
        optimize_collision_model_freq=1,

        multipliers_lr=1,
        collision_multipliers_lr=1
    )
)

test_environment = TestEnvironmentBuilder().make_highway_environment(car_obstacle_length=2, car_obstacle_width=5,
                                                                     car_center_x=17, car_center_y=0.1,
                                                                     start_point=np.array(
                                                                         [0, 0, 0], dtype=np.float32),
                                                                     goal_point=np.array(
                                                                         [58, 0, 0], dtype=np.float32,),
                                                                     trajectory_boundaries=np.array([-1, 80, -12, 12]))

obstacle_points = test_environment.obstaclePoints
ego_length = 6
ego_width = 3
collision_checker = CarCollisionChecker(
    (-ego_length/2, ego_length/2, -ego_width/2, ego_width/2), (0, 78, -11, 11))
collision_checker.update_obstacle_points(test_environment.obstaclePoints)

planner = PlannerFactory.make_constrained_onf_planner(
    collision_checker, planner_parameters)
# planner = PlannerFactory.make_onf_planner(collision_checker)

goal_point = test_environment.goalPoint
start_point = test_environment.startPoint
trajectory_boundaries = test_environment.bounds

planner.init(start_point, goal_point, trajectory_boundaries)
device = planner._device
collision_model = planner._collision_model

plotter = Plotter(model=collision_model, boundaries=trajectory_boundaries,
                  obstacles=test_environment.obstaclePoints, trajectory=planner.get_path())

start = time.time()

print("Started planner")
exit_request = False


def run_planner():
    while (not exit_request):
        planner.step(optimize_collision=True, optimize_trajectory=True)
        test_environment.obstaclePoints[:,
                                         0] = test_environment.obstaclePoints[:, 0] + 2.0
        collision_checker.update_obstacle_points(
            test_environment.obstaclePoints)


runner = Thread(target=run_planner, daemon=True)
runner.start()

for i in range(1000):
    start = time.time()
    tr = planner.get_path()

    plotter.update_plotter_data(model=planner._collision_model,
                                trajectory=tr,
                                obstacles=test_environment.obstaclePoints)
    plotter.drawnow()
    print(f"Planner run took: {time.time() - start} seconds")

exit_request = True
runner.join()

input()
