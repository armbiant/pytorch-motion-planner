import rclpy
from rclpy.node import Node
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Pose, Point, Quaternion, Twist, Vector3
from sim_interface import SimInterface as SimInterface
import numpy as np


class SimNode(Node):

    def __init__(self, sim: SimInterface, rate=20):
        super().__init__('sim_node')
        self._sim = sim
        self._vehicles = sim.getCurrentCarNames()
        self._vehicle_pose_pubs = dict()
        self._vehicle_twist_pubs = dict()
        self.traj_coord = None
        for vehicle in self._vehicles:
            self._vehicle_pose_pubs[vehicle] = self.create_publisher(
                Pose, (vehicle+'_pose'), 10)
            self._vehicle_twist_pubs[vehicle] = self.create_publisher(
                Twist, (vehicle+'_twist'), 10)

        self._timer_pub_states = self.create_timer(
            1/rate, self.pub_vehicles_state)
        self._timer_pub_traj = self.create_timer(
            1/100, self.pub_sim_plot_trajectory)

        self.sub_plot_traj = self.create_subscription(Float32MultiArray, 'plot_trajectory',
                                                      self.sub_plot_trajectory, 10)

    def sub_plot_trajectory(self, msg):
        flat = msg.data
        dim = msg.layout.dim
        self.traj_coord = np.array([flat[i:i+dim[1].size]
                                   for i in range(0, dim[0].stride, dim[1].size)])

    def pub_vehicles_state(self):
        for vehicle in self._vehicles:
            p, q = self._sim.get_vehicle_pose(self._sim.vehicles[vehicle])
            v = self._sim.get_vehicle_velocity(self._sim.vehicles[vehicle])
            pose = Pose(position=Point(x=p[0], y=p[1], z=p[2]),
                        orientation=Quaternion(x=q[0], y=q[1], z=q[2], w=q[3]))
            self._vehicle_pose_pubs[vehicle].publish(pose)
            twist = Twist(linear=Vector3(
                x=v[0], y=v[1], z=v[2]), angular=Vector3(x=0.0, y=0.0, z=0.0))
            self._vehicle_twist_pubs[vehicle].publish(twist)

    def pub_sim_plot_trajectory(self):
        if (self.traj_coord is not None):
            self._sim.remove_debug_polylines(self._sim.debug_poly_ids)
            self._sim.plot_debug_polylines(self.traj_coord)
            self._sim.remove_debug_spheres(self._sim.debug_sphere_ids)
            self._sim.plot_debug_spheres(self.traj_coord)


def main(args=None):
    rclpy.init(args=None)
    sim = SimInterface()
    sim.start(simple_logging=False)
    print("Connected to sim")
    node = SimNode(sim)
    rclpy.spin(node)
    sim.close()
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
