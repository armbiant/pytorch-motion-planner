import rclpy
from rclpy.node import Node
import time
from std_msgs.msg import String

from std_msgs.msg import Float32MultiArray, MultiArrayDimension, MultiArrayLayout, Empty
from geometry_msgs.msg import Pose, Point, Quaternion, Transform, Vector3
import numpy as np
class MinimalNode(Node):

    def __init__(self):
        super().__init__('minimal_publisher')
        
        self.pub_plot_traj = self.create_publisher(Float32MultiArray, 'plot_trajectory', 10)

        self.sub_dummy_states = self.create_subscription(Pose, 'dummy_pose',
                                        self.sub_get_vehicle_states, 1)
        rate = 100  # Hz
        # self.timer = self.create_timer(1/rate, self.draw_trajectory)
        self.mr_pub = self.create_publisher(Float32MultiArray, 'multiarray', 10)

        self.i = 0
        self.vehicle_state = []

    def draw_trajectory(self):
        if (len(self.vehicle_state) > 0):
            pos = self.vehicle_state
            data = np.array([[pos[0] - 2, pos[1] -2, pos[2] +1],
                            [pos[0] - 4, pos[1] -4, pos[2] +2],
                            [pos[0] - 6, pos[1] -6, pos[2] +3],
                            [pos[0] - 8, pos[1] -8, pos[2] +2]])

            row = MultiArrayDimension(label="row", size=len(data), stride=len(data)*len(data[0]))
            col = MultiArrayDimension(label="col", size=len(data[0]), stride=len(data[0]))
            lay = MultiArrayLayout(dim=[row, col], data_offset=0)
            msg = Float32MultiArray(layout=lay, data=[item for sublist in data for item in sublist])
            self.pub_plot_traj.publish(msg)

    def sub_get_vehicle_states(self, msg):
        print([msg.position.x, msg.position.y, msg.position.z])
        time.sleep(10)
        

    def example(self):
        msg = String()
        msg.data = 'Hello World: %d' % self.i
        self.publisher_.publish(msg)

        arr = [[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]]
        row = MultiArrayDimension(label="row", size=3, stride=3)
        col = MultiArrayDimension(label="col", size=2, stride=3*2)
        lay = MultiArrayLayout(dim=[row, col], data_offset=0)


        flatten = [item for sublist in arr for item in sublist]
        lay = MultiArrayLayout(dim=[col, row], data_offset=0)
        _mr = Float32MultiArray(layout=lay, data=flatten)
        self.mr_pub.publish(_mr)
        self.get_logger().info('Publishing: "%s"' % _mr.data)
        dim = _mr.layout.dim
        flatten = _mr.data
        unflatten = [flatten[i:i+dim[1].size] for i in range(0, dim[0].stride, dim[1].size)]
        print(unflatten)
        print(unflatten[0][0])

        arr = [2.0, 3.0, 4.0]
        row = MultiArrayDimension(label="row", size=len(arr), stride=len(arr))
        lay = MultiArrayLayout(dim=[row], data_offset=0)
        _mm = Float32MultiArray(layout=lay, data=arr)
        print(_mm.data)
        self.i += 1


def main(args=None):
    
    rclpy.init(args=args)
    
    minimal_publisher = MinimalNode()

    rclpy.spin(minimal_publisher)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()