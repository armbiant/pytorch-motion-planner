from beamngpy import BeamNGpy, Scenario, Vehicle, set_up_simple_logging
from math import sqrt
from dataclasses import dataclass
import typing as tp
import numpy as np
from beamngpy.logging import BNGError
from pytorch_lightning.utilities import AttributeDict
from scene_config import Scene
import string
import msgpack
@dataclass
class Color():
    white = [1.0, 1.0, 1.0, 1.0]
    yellow = [1.0, 0.847, 0.01, 1.0]
    red = [0.9, 0.01, 0.01, 0.9]
    black = [0.0, 0.00, 0.00, 1.0]
    skyblue = [0.529, 0.807, 0.921, 1.0]



class SimInterface():

    def __init__(self, host='localhost', port=64257, home='C:\BeamNG.tech.v0.27.0.0', scene: AttributeDict = Scene.cross):
        self._beamng = BeamNGpy(host=host, port=port, home=home)
        self._scenario = Scenario('west_coast_usa', 'nfomp_test')
        self.vehicles = {
            "dummy": Vehicle('dummy', model='citybus', licence='DUMMY'),
            "ego": Vehicle('ego', model='etk800', licence='EGO')
        }
        self._scene = scene
        self._scenario.add_vehicle(
            self.vehicles["ego"], pos=self._scene.ego_pose.pos, rot_quat=self._scene.ego_pose.rotation)
        self._scenario.add_vehicle(
            self.vehicles["dummy"], pos=self._scene.dummy_pose.pos, rot_quat=self._scene.dummy_pose.rotation)
        self.debug_poly_ids = []
        self.debug_sphere_ids = []
        self.debug_rect_ids = []
        self.debug_heatmap_sphere_ids = []
        self.running = False
        self.debug_spheres_launch_index = None
        self.debug_spheres_last_index = None

    def start(self, simple_logging=False):
        set_up_simple_logging() if simple_logging else None
        self._beamng.open()
        if not self.running:
            self._scenario.make(self._beamng)
            self._beamng.load_scenario(self._scenario)
            self._beamng.start_scenario()
            self.reset_vehicles()
            self.running = True

    @staticmethod
    def create_sim_interface_instance(host='localhost', port=64257, home='C:\BeamNG.tech.v0.27.0.0') -> "SimInterface":
        """
        An instance of BeamNG.tech simulator must be launched beforehand
        """
        sim = SimInterface(host=host, port=port, home=home)
        try:
            sim._beamng = sim._beamng.open(launch=False)
            print("[Sim Interface] Sucessfully connected to sim")
        except BNGError:
            print("[Sim Interface] No simulation instance is running")
            return None
        return sim

    def reconnect_instance_to_vehicle(self, vehicle: Vehicle):
        self._beamng.open(launch=False)
        vehicle.connect(self._beamng)

    def reconnect_instance_to_all_vehicles(self):
        self._beamng.open(launch=False)
        self.vehicles = self._beamng.get_current_vehicles()
        for vehicle in self.vehicles:
            self.vehicles[vehicle].connect(self._beamng)
        print("[Sim Interface] Reconnected instance to all vehicles")

    def get_current_vehicles(self) -> dict():
        return self.vehicles

    def getCurrentCarNames(self):
        return [key for key in self.vehicles]

    def reset_vehicles(self):
        self.vehicles["ego"].teleport(
            pos=self._scene.ego_pose.pos, rot_quat=self._scene.ego_pose.rotation)
        self.vehicles["dummy"].teleport(
            pos=self._scene.dummy_pose.pos, rot_quat=self._scene.dummy_pose.rotation)
        self.vehicles["ego"].set_color(rgba=Color.skyblue)
        self.vehicles["dummy"].set_color(rgba=Color.red)

    def disable_ai(self):
        for vehicle in self.vehicles:
            self.vehicles[vehicle].ai_set_mode('disabled')        

    def reset_scene(self):
        self._beamng.restart_scenario()

    def sendScript(self, vehicle:string, script):
        assert all(all(k in node for k in ('x', 'y', 'z', 't')) for node in script), "Wrong AI script passed"
        ai_script = []
        if isinstance(script, np.ndarray):
            script = script.tolist()
        for pose in script:
            node = {
                'x': pose['x'],
                'y': pose['y'],
                'z': pose['z']+0.1,
                't': pose['t']
            }
            ai_script.append(node)
        try:
            self.vehicles[vehicle].ai_set_aggression(10.0)
            self.vehicles[vehicle].ai_set_script(ai_script, cling = True)
        except msgpack.exceptions.ExtraData:
            print("[SimInterface] Error raised while setting ai script")
            pass

    @staticmethod
    def get_vehicle_length_and_width(vehicle: Vehicle):
        bbox = vehicle.get_bbox()
        width = sqrt((bbox['rear_bottom_left'][0] - bbox['rear_bottom_right'][0])**2 +
                     (bbox['rear_bottom_left'][1] - bbox['rear_bottom_right'][1])**2 +
                     (bbox['rear_bottom_left'][2] - bbox['rear_bottom_right'][2])**2)

        length = sqrt((bbox['rear_bottom_left'][0] - bbox['front_bottom_left'][0])**2 +
                      (bbox['rear_bottom_left'][1] - bbox['front_bottom_left'][1])**2 +
                      (bbox['rear_bottom_left'][2] - bbox['front_bottom_left'][2])**2)
        return length, width

    def get_distance_btw_vehicles(self, vehicle1: Vehicle, vehicle2: Vehicle):
        pos1 = self.get_vehicle_pos(vehicle1)
        pos2 = self.get_vehicle_pos(vehicle2)
        dx = pos2[0] - pos1[0]
        dy = pos2[1] - pos1[1]
        dz = pos2[2] - pos1[2]
        distance = sqrt(dx**2 + dy**2 + dz**2)
        return (dx, dy, dz), distance

    # TODO delete; replaced by beamng.compute_rotation_matrix
    def get_vehicle_rotmat(self, vehicle: Vehicle) -> tp.List[tp.List]:
        vehicle.poll_sensors()
        dir = vehicle.state['dir']
        up = vehicle.state['up']
        left = np.cross(up, dir)
        return np.transpose(np.stack((dir, left, up), axis=0))

    def get_roads(self) -> dict:
        return self._beamng.get_roads()

    def get_vehicle_pose(self, vehicle: Vehicle) -> tp.List:
        vehicle.poll_sensors()
        return self._get_vehicle_pos(vehicle), self._get_vehicle_rot_quat(vehicle)

    def get_vehicle_velocity(self, vehicle: Vehicle) -> tp.List:
        vehicle.poll_sensors()
        return vehicle.state['vel']

    def _get_vehicle_rot_quat(self, vehicle: Vehicle) -> tp.List:
        return vehicle.state['rotation']

    def _get_vehicle_pos(self, vehicle: Vehicle) -> tp.List:
        return vehicle.state['pos']

    def plot_debug_polylines(self, poly_coord: np.ndarray):
        assert poly_coord.size != 0
        assert len(poly_coord.shape) == 2
        assert poly_coord.shape[1] == 3
        yellow = [1.0, 0.847, 0.01, 0.8]
        poly_id = self._beamng.add_debug_polyline(
            coordinates=poly_coord.tolist(), rgba_color=yellow, cling=True)
        self.debug_poly_ids.append(poly_id)

    def plot_debug_spheres(self, sphere_coord: np.ndarray):
        assert sphere_coord.size != 0
        assert len(sphere_coord.shape) == 2
        assert sphere_coord.shape[1] == 3
        radii = len(sphere_coord)*[0.26]
        colors = len(sphere_coord)*[Color.yellow]
        coords = sphere_coord.tolist()
        assert len(coords) == len(radii) == len(colors)
        sphere_ids = self._beamng.add_debug_spheres(
            coordinates=coords, radii=radii, rgba_colors=colors, cling=True)
        if (self.debug_spheres_launch_index is None):
            self.debug_spheres_launch_index = sphere_ids[0]
        self.debug_spheres_last_index = sphere_ids[-1]
        self.debug_sphere_ids.append(sphere_ids)
        return sphere_ids

    def plot_debug_rectangles(self, rect_coord: np.ndarray, color_arr: np.ndarray):
        assert rect_coord.size != 0
        assert len(rect_coord.shape) == 3
        assert rect_coord.shape[1] == 4
        assert rect_coord.shape[2] == 3

        color_arr = np.reshape(color_arr, ((rect_coord.shape[0], 4)))

        for i in range(len(rect_coord)):
            coords = rect_coord[i].tolist()
            color_arr[i][3] = 0.75
            color = color_arr[i].tolist()
            rect_ids = self._beamng.add_debug_rectangle(
                vertices=coords, rgba_color=color, cling=True, offset=0.1)
            self.debug_rect_ids.append(rect_ids)

    def plot_debug_heatmap_spheres(self, sphere_coord: np.ndarray, color_arr: np.ndarray, offset=0.1):
        assert sphere_coord.size != 0
        assert len(sphere_coord.shape) == 2
        assert sphere_coord.shape[1] == 3

        color_arr = np.reshape(color_arr, ((sphere_coord.shape[0], 4)))

        radii = len(sphere_coord)*[0.3]
        coords = sphere_coord.tolist()
        color_arr = [[c[0], c[1], c[2], 0.55] for c in color_arr]
        colors = color_arr
        assert len(coords) == len(radii) == len(colors)
        ids = self._beamng.add_debug_spheres(
            coordinates=coords, radii=radii, rgba_colors=colors, cling=True, offset=offset)
        self.debug_heatmap_sphere_ids.append(ids)
        return ids

    def remove_debug_heatmap_spheres(self, sphere_ids: tp.List):
        self._beamng.remove_debug_spheres(sphere_ids)
        self.debug_heatmap_sphere_ids = [
            e for e in self.debug_heatmap_sphere_ids if e not in sphere_ids]

    def remove_debug_polylines(self, poly_ids: tp.List):
        for poly_id in poly_ids:
            self._beamng.remove_debug_polyline(poly_id)
        self.debug_poly_ids = [
            e for e in self.debug_poly_ids if e not in poly_ids]

    def remove_debug_spheres(self, sphere_ids: tp.List):
        self._beamng.remove_debug_spheres(sphere_ids)
        self.debug_sphere_ids = [
            e for e in self.debug_sphere_ids if e not in sphere_ids]

    def remove_debug_rectangles(self, rect_ids: tp.List):
        for rect_id in rect_ids:
            self._beamng.remove_debug_rectangle(rect_id)
        self.debug_rect_ids = [
            e for e in self.debug_rect_ids if e not in rect_ids]
    
    def remove_all_debug_graphics(self):
        self.remove_debug_rectangles(self.debug_rect_ids)
        self.remove_debug_spheres(self.debug_sphere_ids)
        self.remove_debug_spheres(list(range(int(self.debug_spheres_launch_index),int(self.debug_spheres_last_index),1)))
        self.remove_debug_polylines(self.debug_poly_ids)
        self.remove_debug_heatmap_spheres(self.debug_heatmap_sphere_ids)

    def close(self):
        self._beamng.close()


def main():
    sim = SimInterface()
    sim.start(simple_logging=False)


if __name__ == "__main__":
    main()
    print("To close the sim hit any button")
    input()
