import sys
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/astar')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/utils')

from multiprocessing import Queue, Event
from neural_field_optimal_planner.onf_model import ONF
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib.collections import QuadMesh
from matplotlib.patches import Polygon
import torch
import time
import queue
import geopandas as gpd
from shapely.geometry import mapping
from shapely.geometry.polygon import Polygon
from shapely.geometry.multipolygon import MultiPolygon
from shapely.geometry.linestring import LineString
from mpl_toolkits.mplot3d.art3d import Line3DCollection, Patch3DCollection, Poly3DCollection, Patch3D    
from scene_config import Scene

class Plotter:

    def __init__(self, name, qModel: Queue, qTraj: Queue, bounds: tuple, qObstacles: Queue, run_flag: Event):
        self.name = name
        self.log = True
        self.qModel: Queue = qModel
        self.qTraj: Queue = qTraj
        self.qObstacles: Queue = qObstacles

        # TODO: nove to configs, check also in planner.py
        self.egoLength = 10  # x
        self.egoWidth = 20  # y
        self.egoHeight = 1  # y

        self.run = run_flag
        self.model = None
        self.trajectory = None # Canvas
        self.trajectory_boxes = None

        self.obstacle_trajectory = None # Canvas
        self.obstacle_trajectory_boxes = None

        self.obstacles = None # Canvas
        self.obstaclePoints = None
        self.trajectoryPoints = None
        self.bounds = bounds

        
    def process(self):

        xmin = self.bounds[0]
        xmax = self.bounds[1]
        ymin = self.bounds[2]
        ymax = self.bounds[3]
        zmin = 0
        zmax = 60

        self.fig = plt.figure(dpi=150)
        self.fig.add_axes([0.02, 0.02, 0.96, 0.96], projection='3d', focal_length=0.2, computed_zorder=True)
        self.ax = self.fig.axes[0]
        # self.ax.set_proj_type('persp', focal_length=70.0)
        self.ax.set_xlim(xmin, xmax)
        self.ax.set_ylim(ymin, ymax)
        self.ax.set_aspect("equal")
        self.ax.set_zlim(zmin, zmax)
        self.heatmap: QuadMesh = None

        self.ax.set_xlabel(xlabel="Y [m]", fontsize=8.0)
        self.ax.set_ylabel(ylabel="X [m]", fontsize=8.0)
        self.ax.set_zlabel(zlabel="t [sec]", fontsize=8.0)
        self.ax.view_init(azim=0, elev=90)

        self.gridsize = 150
        grid_x, grid_y = np.meshgrid(np.linspace(xmin, xmax, self.gridsize),
                                     np.linspace(ymin, ymax, self.gridsize))
        self.grid = np.stack(
            [grid_x, grid_y, np.zeros_like(grid_x)], axis=2).reshape(-1, 3)
        self.mesh = self.grid.reshape(self.gridsize, self.gridsize, 3)

        self.flags = {"polygon_map": False, "obstacle_path": False}
        
        print(f"[Plotter] [{self.name}] Initialized")

        while(1):
            try:
                self.model = self.qModel.get(block=False)
                self.trajectoryPoints = self.qTraj.get(block=False)
                self.obstaclePoints = self.qObstacles.get(block=False)
                print(f"[Plotter] [{self.name}] Got data in queues")  if self.log else None
                break
            except queue.Empty:
                time.sleep(1)
                continue

        while (not self.run.is_set()):
            self.update_plotter_data()
            # # Matplotlib 2D plot
            self.update_canvas_objects()
            self.drawnow()
        
        print("[Plotter] Cleanup")
        plt.close()
        print("[Plotter] Shutdown")

        
    def drawnow(self):
        plt.draw()
        plt.pause(0.001)
        # self.fig.show()
        # self.fig.canvas.draw()
        # self.fig.canvas.flush_events()

    def update_plotter_data(self):
        if self.qModel.empty():
            pass
        else:
            self.model = self.qModel.get()
            assert isinstance(
                self.model, ONF), f"[Plotter] [{self.name}] Type error in model queue assertion"

        if self.qTraj.empty():
            pass
        else:
            self.trajectoryPoints = self.qTraj.get()
            assert isinstance(
                self.trajectoryPoints, np.ndarray), f"[Plotter] [{self.name}] Type error in trajectory queue assertion"

        if self.qObstacles.empty():
            pass
        else:
            self.obstaclePoints = self.qObstacles.get()
            assert (isinstance(
                self.obstaclePoints, np.ndarray) or isinstance(
                self.obstaclePoints, gpd.GeoSeries)), f"[Plotter] [{self.name}] Type error in obstacles queue assertion"

    def update_canvas_objects(self):
        # legacy
        # if (isinstance(self.obstaclePoints, np.ndarray)):
        #    self.plot_obstacles()

        self.plot_trajectory()
        self.plot_obstacles_path()
        if (isinstance(self.obstaclePoints, gpd.GeoSeries)):
            self.plot_polygons()
        self.plot_model_heatmap()

    def get_heatmap(self) -> QuadMesh:
        if self.heatmap is not None:
            return self.heatmap

    def plot_model_heatmap(self, device="cpu"):
        obstacle_probabilities = self.model(torch.tensor(
            self.grid.astype(np.float32), device=device))
        obstacle_probabilities = obstacle_probabilities.cpu(
        ).detach().numpy().reshape(self.gridsize, self.gridsize)
        cmap = mpl.cm.get_cmap('Blues')
        fcolors = cmap([1-prob for prob in obstacle_probabilities])
        if (self.heatmap is not None):
            self.heatmap.remove()
        self.heatmap = self.ax.plot_surface(
                X=np.array(self.mesh[:, :, 0]),
                Y=np.array(self.mesh[:, :, 1]),
                Z=np.zeros_like(self.mesh[:,:,0]),
                facecolors=fcolors,
                alpha=1.0,
                zorder = -1000)
        
    def plot_trajectory(self):
        points = self.trajectoryPoints
        heights = np.linspace(0,40,len(points))

        if (self.trajectory is not None):
            self.trajectory.remove()
        self.trajectory = self.ax.scatter(np.array(points[:, 0]),
                                        np.array(points[:, 1]),
                                        heights,
                                        color="yellow",
                                        edgecolors="black",
                                        linewidths=0.5,
                                        alpha=1.0, 
                                        s=4,
                                        zorder = 1000)
        w = float(self.egoWidth/2)
        l = float(self.egoLength/2)
        h = float(self.egoHeight/2)
        #vertices
        v = [[-w, -l, -h],[w, -l, -h],[w, l, -h],[-w, l, -h],
                [-w, -l, h],[w, -l, h],[w, l, h],[-w, l, h]]

        points = points[::4]
        heights = heights[::4]

        series_list = [gpd.GeoSeries(MultiPolygon([Polygon([v[0],v[1], v[2], v[3]]),
                                            Polygon([v[4],v[5], v[6], v[7]]),
                                            Polygon([v[0],v[4], v[7], v[3]]),
                                            Polygon([v[0],v[1], v[5], v[4]]),
                                            Polygon([v[1],v[2], v[6], v[5]]),
                                            Polygon([v[2],v[3], v[7], v[6]])
                                            ]))
                                            .translate(xyt[0], xyt[1], z)
                                            .rotate(xyt[2], origin="center", use_radians=True)
                                            for xyt, z in zip(points,heights)]

        pcs = []
        for geoseries in series_list:
            vertices = [list(x.exterior.coords) for x in geoseries[0].geoms]
            pc = []
            for plane in vertices:
                pc.append(Poly3DCollection(np.array([plane]), facecolors="deepskyblue", edgecolors="black", linewidths=0.5, alpha = 0.6, zorder = 1000))
            pcs.append(pc)
        if self.trajectory_boxes is not None:
            [[poly.remove() for poly in pc] for pc in self.trajectory_boxes]
        self.trajectory_boxes = [[self.ax.add_collection(poly) for poly in pc] for pc in pcs]
            

    def plot_obstacles(self):
        points = self.obstaclePoints
        if (points is not None):
            if (self.obstacles is None):
                self.obstacles = self.ax.scatter(points[:, 0], points[:, 1], zs=0, zdir='z', color="black", s=1)
            else:
                self.obstacles.set_offsets(points)
    
    def plot_polygons(self):
        if (self.flags["polygon_map"]):
            return
        polys_ext = [[elem for elem in [list(x.exterior.coords) for x in multipoly.geoms]] for multipoly in self.obstaclePoints]
        polys_int = [[elem for elem in [list([i.coords for i in x.interiors]) for x in multipoly.geoms]] for multipoly in self.obstaclePoints]

        pc = []
        for line in polys_ext[0]:
            line = np.array([[p[0], p[1], 0.] for p in line])
            pc.append(Poly3DCollection(np.expand_dims(line, axis=0), facecolors=(0.,0.,0.,0.), edgecolors="black", linewidths=1.5, zorder = -100))
        for poly in polys_int[0]:
            for line in poly:
                line = np.array([[p[0], p[1], 0.] for p in line])
                pc.append(Poly3DCollection(np.expand_dims(line, axis=0), facecolors=(0.,0.,0.,0.), edgecolors="black", linewidths=1.5, zorder = -100))
        [self.ax.add_collection(polyline) for polyline in pc]
        self.flags["polygon_map"] = True
    
    def plot_obstacles_path(self):
        if (self.flags["obstacle_path"]):
            return
        points = np.vstack([[pose['x'], pose['y'], pose['theta']] for pose in Scene.cross.dummy_line_script])
        heights = np.vstack([pose['t'] for pose in Scene.cross.dummy_line_script])

        if (self.obstacle_trajectory is not None):
            self.obstacle_trajectory.remove()
        self.obstacle_trajectory = self.ax.scatter(np.array(points[:, 0]),
                                                np.array(points[:, 1]),
                                                np.array(heights),
                                                color="magenta",
                                                edgecolors="black",
                                                linewidths=0.5,
                                                alpha=1.0, 
                                                s=4,
                                                zorder = 1000)

        w = float(self.egoWidth/2)
        l = float(self.egoLength/2)
        h = float(self.egoHeight/2)
        #vertices
        v = [[-w, -l, -h],[w, -l, -h],[w, l, -h],[-w, l, -h],
                [-w, -l, h],[w, -l, h],[w, l, h],[-w, l, h]]

        points = points[::3]
        heights = heights[::3]
        
        series_list = [gpd.GeoSeries(MultiPolygon([Polygon([v[0],v[1], v[2], v[3]]),
                                            Polygon([v[4],v[5], v[6], v[7]]),
                                            Polygon([v[0],v[4], v[7], v[3]]),
                                            Polygon([v[0],v[1], v[5], v[4]]),
                                            Polygon([v[1],v[2], v[6], v[5]]),
                                            Polygon([v[2],v[3], v[7], v[6]])
                                            ]))
                                            .translate(xyt[0], xyt[1], z)
                                            .rotate(xyt[2], origin="center", use_radians=True)
                                            for xyt, z in zip(points,heights)]

        pcs = []
        for geoseries in series_list:
            vertices = [list(x.exterior.coords) for x in geoseries[0].geoms]
            pc = []
            for plane in vertices:
                pc.append(Poly3DCollection(np.array([plane]), facecolors="red", edgecolors="black", linewidths=0.5, alpha = 0.6, zorder = 1000))
            pcs.append(pc)
        if self.obstacle_trajectory_boxes is not None:
            [[poly.remove() for poly in pc] for pc in self.obstacle_trajectory_boxes]
        self.obstacle_trajectory_boxes = [[self.ax.add_collection(poly) for poly in pc] for pc in pcs]


        self.flags["obstacle_path"] = True
        pass

