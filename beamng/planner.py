import sys
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/astar')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/utils')

from sim_interface import SimInterface as SimInterface
import torch
import numpy as np
from multiprocessing import Queue, Event
import queue
from geometry_msgs.msg import Point, Transform, Vector3
from matplotlib.collections import QuadMesh
from beamngpy import quat
from neural_field_optimal_planner.collision_checker import CarCollisionChecker, PolygonCollisionChecker
from neural_field_optimal_planner.planner_factory import PlannerFactory
from neural_field_optimal_planner.plotting_utils import *
from neural_field_optimal_planner.test_environment_builder import TestEnvironmentBuilder
from planner_utils import *
from scene_config import Scene, DEFAULT_PARAMS
import time
from copy import deepcopy

class Planner:
    def __init__(self, sim: SimInterface, qCarPoses: dict, qTraj: dict, qModel: Queue, qObstacles: Queue, qScript: dict, run_flag: Event):
        self.ready = False
        self.log = False
        self.run = run_flag
        self.traj = None
        self.sim = sim
        # TODO: nove to configs, check also in planner.py
        self.egoLength = 4  # x
        self.egoWidth = 2.1  # y
        self.planner = None
        self.plannerParams = None
        self.testEnv = None
        self.collisionChecker = None
        self.dummyBbox = [4.7, 2.1, 0.0]
        self.carPoses = dict()
        self.carNames = sim.getCurrentCarNames()
        
        self.qCarPoses = qCarPoses
        print("[Planner] Waiting for data in queues")
        for car in self.qCarPoses:
            self.carPoses[car] = qCarPoses[car].get(block=True)
        print("[Planner] Got data in queues")
        self.qTraj = qTraj
        self.qObstacles = qObstacles
        self.qModel = qModel        
        self.qScript = qScript
        self.params = DEFAULT_PARAMS
        dummyPos = self.getDummyRelPos()

        print("[Planner] Start making environment")
        
        self.testEnv = \
            TestEnvironmentBuilder().make_environment_gpd_polygon(roads=np.load("beamng/roads.npy", allow_pickle=True),
                                                                    bounds=Scene.cross.trajectory_boundaries)

        # self.testEnv = \
        #     TestEnvironmentBuilder().make_highway_environment(car_obstacle_length=self.dummyBbox[1],
        #                                                       car_obstacle_width=self.dummyBbox[0],
        #                                                       car_center_x=dummyPos.x,
        #                                                       car_center_y=dummyPos.y,
        #                                                       start_point=Scene.cross.start_point,
        #                                                       goal_point=Scene.cross.goal_point,
        #                                                       trajectory_boundaries=Scene.cross.trajectory_boundaries)

        print("[Planner] Made environment")
        # self.collsisionChecker = CarCollisionChecker(box=(-self.egoLength/2,
        #                                                   self.egoLength/2,
        #                                                   -self.egoWidth/2,
        #                                                   self.egoWidth/2),
        #                                              collision_boundaries=Scene.cross.trajectory_boundaries)

        # self.collsisionChecker.update_obstacle_points(self.testEnv.obstaclePoints)

        self.collsisionChecker = PolygonCollisionChecker(box=(-self.egoLength/2, self.egoLength/2,
                                                          -self.egoWidth/2, self.egoWidth/2),
                                                          polygon_map=self.testEnv.obstaclePoints,
                                                     collision_boundaries=Scene.cross.trajectory_boundaries)

        print("[Planner] Made checker")

        self.planner = PlannerFactory.make_constrained_onf_planner(
            self.collsisionChecker, self.params)

        self.planner.init(self.testEnv.startPoint,
                          self.testEnv.goalPoint,
                          self.testEnv.bounds)
        self.ready = True
        print("[Planner] Initialized")

    def process(self):
        print("[Planner] Started")
        iter = 0
        while (not self.run.is_set()):
            for name in self.carNames:
                try:
                    self.carPoses[name] = self.qCarPoses[name].get(block=False)
                except queue.Empty:
                    continue

            start = time.time()
            self.updatePlannerEnvironment()
            self.planner.step()
            try:
                model = deepcopy(self.planner._collision_model)
                self.qModel.put(model, block=False)
            except queue.Full:
                pass
            traj = self.planner.get_full_trajectory()
            
            try:
                self.qTraj["plotter"].put(traj, block=False)
            except queue.Full:
                pass

            try:
                self.qTraj["sim"].put(traj, block=False)
            except queue.Full:
                pass

            try:
                self.qObstacles.put(self.testEnv.obstaclePoints, block=False)
            except queue.Full:
                pass
            
            print(f"[Planner] Run took: {time.time() - start} seconds")
            iter += 1
            if (iter == 250):
                print(f"[Planner] Sending scripts to sim")
                self.qScript["dummy"].put(Scene.cross.dummy_line_script)
                print(f"[Planner] Sending scripts to sim")
                self.qScript["ego"].put(self.getScriptFromTrajectory())
        
        print("[Planner] Cleanup")
        print("[Planner] Shutdown")

    def getScriptFromTrajectory(self) -> list:
        script = []
        tr = self.planner.get_full_trajectory()
        time = np.linspace(0,34,len(tr))
        i = 0
        for node in zip(tr,time):
            if (i < 30):
                z = 92.
            else:
                z = 400.
            script.append({'x':float(node[0][0]),
                            'y':float(node[0][1]),
                            'z':float(z),
                            't':float(node[1]),
                            'theta':float(node[0][2])})
            i += 1
        return script

    def getDummyRelPos(self) -> Point:
        tf_ego = Transform(translation=point_to_vector3(self.carPoses["ego"].position),
                           rotation=self.carPoses["ego"].orientation)
        tf_dummy = Transform(translation=point_to_vector3(self.carPoses["dummy"].position),
                             rotation=self.carPoses["dummy"].orientation)
        rel_transform = Vector3(x=tf_dummy.translation.x - tf_ego.translation.x,
                                y=tf_dummy.translation.y - tf_ego.translation.y,
                                z=tf_dummy.translation.z - tf_ego.translation.z)
        rel_transform = rotate_point_by_quaternion(
            p=rel_transform, q=tf_ego.rotation)
        return Point(x=rel_transform.x, y=rel_transform.y, z=rel_transform.z)

    def getTargetPos(self) -> Point:
        tf_ego = Transform(translation=point_to_vector3(self.carPoses["ego"].position),
                           rotation=self.carPoses["ego"].orientation)
        tf_dummy = Transform(translation=point_to_vector3(self.carPoses["dummy"].position),
                             rotation=self.carPoses["dummy"].orientation)
        goal = self.testEnv.goalPoint
        
        p1 = rotate_point_by_quaternion(p=Point(x=goal[0].item(),
                                                y=goal[1].item(),
                                                z=goal[2].item()),
                                        q=quaternion_conjugate(tf_dummy.rotation))

        p2 = Vector3(x=tf_dummy.translation.x + p1.x,
                     y=tf_dummy.translation.y + p1.y,
                     z=tf_dummy.translation.z + p1.z)
        tf_target = Transform(
            translation=p2, rotation=self.carPoses["dummy"].orientation)
        rel_transform = Vector3(x=tf_target.translation.x - tf_ego.translation.x,
                                y=tf_target.translation.y - tf_ego.translation.y,
                                z=tf_target.translation.z - tf_ego.translation.z)
        rel_transform = rotate_point_by_quaternion(
            p=rel_transform, q=self.carPoses["ego"].orientation)
        return Point(x=rel_transform.x, y=rel_transform.y, z=rel_transform.z)

    def trajToGlobal(self, traj: np.ndarray) -> np.ndarray:
        pos = self.carPoses["ego"].position
        quat = self.carPoses["ego"].orientation
        rotated = np.insert(traj, 0, [0.0, 0.0, 0.0], axis=0)
        for i in range(len(rotated)):
            x = rotated[i][0].item()
            y = rotated[i][1].item()
            z = 0.0
            p = rotate_point_by_quaternion(p=point_to_vector3(
                Point(x=x, y=y, z=z)), q=quaternion_conjugate(quat))
            rotated[i] = np.add(np.array([p.x, p.y, 119.0]),
                                np.array([pos.x, pos.y, 0.0]))
        return rotated

    def updatePlannerEnvironment(self):
        pos = self.getDummyRelPos()
        q1 = self.carPoses["ego"].orientation
        q2 = self.carPoses["dummy"].orientation
        q = quaternion_mult(quaternion_conjugate(q2), q1)
        rm = quat.compute_rotation_matrix([q.x, q.y, q.z, q.w])
        obstacle_points = TestEnvironmentBuilder.beamng_car_obstacle_points(length=self.dummyBbox[0],
                                                                            width=self.dummyBbox[1],
                                                                            cx=pos.x,
                                                                            cy=pos.y,
                                                                            rot=rm)
        start = Scene.cross.start_point
        relPos = self.getTargetPos()
        goal = np.array([relPos.x, relPos.y, 0.0])
        # Update in planner
        # self.collsisionChecker.update_obstacle_points(obstacle_points)
        self.planner.update_start_point(start)
        # self._planner.update_goal_point(goal)
        # Update in test environment
        self.testEnv.goalPoint = goal
        self.testEnv.startPoint = start
        # self.testEnv.obstaclePoints = obstacle_points
