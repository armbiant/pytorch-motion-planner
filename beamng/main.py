
import sys
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/astar')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/utils')

from scene_config import Scene
from planner_utils import *
from dataclasses import dataclass

from multiprocessing import Process, Queue, Event
import numpy as np
import torch
import time
from pytorch_lightning.utilities import AttributeDict
from sim_interface import SimInterface
from sim import Sim
from plotter import Plotter
from planner import Planner


torch.random.manual_seed(100)
np.random.seed(400)

trajectories = {
    "ego": {
        "plotter": Queue(1),
        "sim": Queue(1)
    },
    "dummy": {
        "plotter": Queue(1),
        "sim": Queue(1)
    }
}
obstacles = {
    "ego": {
        "plotter": Queue(1)
    },
    "dummy": {
        "plotter": Queue(1)
    }
}
scripts = {
    "ego": Queue(1),
    "dummy": Queue(1)
}
model = {
    "ego": Queue(1)
}
poses = {
    "ego_planner" : {
        "ego": Queue(1),
        "dummy": Queue(1)
    }
}

processes = {
    "planner": None,
    "plotter": None,
    "sim": None,
    "beamng": None  # TODO нужно ли вообще?
}

run_flags = {
    "planner": Event(),
    "plotter": Event(),
    "sim": Event()
}

def main():

    interface = SimInterface.create_sim_interface_instance()
    if interface is None:
        interface = SimInterface()
        interface.start()
    interface.reconnect_instance_to_all_vehicles()
    interface.disable_ai()
    interface.reset_vehicles()

    sim = Sim(sim=interface,
              qCarPoses=poses["ego_planner"],
              qTraj=trajectories["ego"]["sim"],
              qScripts=scripts,
              run_flag=run_flags["sim"])
    processes["sim"] = Process(target=sim.process, args=(), daemon=True)
    
    planner = Planner(sim=interface,
                      qCarPoses=poses["ego_planner"],
                      qTraj=trajectories["ego"],
                      qModel=model["ego"],
                      qObstacles=obstacles["ego"]["plotter"],
                      qScript=scripts,
                      run_flag=run_flags["planner"])
    processes["planner"] = Process(target=planner.process, args=(), daemon=True)
    
    plt = Plotter(name="global",
                  qModel=model["ego"],
                  qTraj=trajectories["ego"]["plotter"],
                  bounds=Scene.cross.trajectory_boundaries,
                  qObstacles=obstacles["ego"]["plotter"],
                  run_flag=run_flags["plotter"])
    processes["plotter"] = Process(target=plt.process, args=(), daemon=True)
    processes["sim"].start()
    processes["planner"].start()
    processes["plotter"].start()

    input()

    for flag in run_flags:
        run_flags[flag].set()
    # Need to wait for cleanup
    processes["sim"].join()
    print("[Main] Shutdown")

if __name__ == '__main__':
    main()
