import numpy as np
from math import sqrt
from geometry_msgs.msg import Pose, Point, Quaternion, Transform, Vector3
from std_msgs.msg import Float32MultiArray, MultiArrayDimension, MultiArrayLayout

def rotate_point_by_quaternion(p:Vector3, q:Quaternion) -> Vector3:
    v = np.array([p.x, p.y, p.z])
    u = np.array([q.x, q.y, q.z])
    s = q.w
    rp = 2.0 * np.dot(u, v) * u + (s*s - np.dot(u, u)) * v + 2.0 * s * np.cross(u, v)
    return Vector3(x=rp[0], y=rp[1], z=rp[2])

def quaternion_conjugate(q:Quaternion) -> Quaternion:
    return Quaternion(x=-q.x, y=-q.y, z=-q.z, w=q.w)

def quaternion_inverse(q:Quaternion) -> Quaternion:
    n = q.x**2 + q.y**2 + q.z**2 + q.w**2
    return Quaternion(x=-q.x/n, y=-q.y/n, z=-q.z/n, w=q.w/n)

def point_to_vector3(p:Point) -> Vector3:
    return Vector3(x=p.x, y=p.y, z=p.z)

def vector3_to_point(p:Vector3) -> Point:
    return Point(x=p.x, y=p.y, z=p.z)

def quaternion_normalize(q:Quaternion) -> Quaternion:
    n = sqrt(q.x**2 + q.y**2 + q.z**2 + q.w**2)
    return Quaternion(x=q.x/n, y=q.y/n, z=q.z/n, w=q.w/n) 

def quaternion_mult(q:Quaternion,r:Quaternion) -> Quaternion:
    q = [q.w, q.x, q.y, q.z]
    r = [r.w, r.x, r.y, r.z]
    mult = [r[0]*q[0]-r[1]*q[1]-r[2]*q[2]-r[3]*q[3],
            r[0]*q[1]+r[1]*q[0]-r[2]*q[3]+r[3]*q[2],
            r[0]*q[2]+r[1]*q[3]+r[2]*q[0]-r[3]*q[1],
            r[0]*q[3]-r[1]*q[2]+r[2]*q[1]+r[3]*q[0]]
    return Quaternion(x=mult[1], y=mult[2], z=mult[3], w=mult[0])

def quaternion_add(q:Quaternion,r:Quaternion) -> Quaternion:
    return Quaternion(x=q.x+r.x, y=q.y+r.y, z=q.z+r.z, w=q.w+r.w)

def quaternion_sub(q:Quaternion,r:Quaternion) -> Quaternion:
    return Quaternion(x=q.x-r.x, y=q.y-r.y, z=q.z-r.z, w=q.w-r.w)

def rotate_point_by_quaternion2(p:Vector3, q:Quaternion) -> Point:
    r = Quaternion(w=0.0, x=p.x, y=p.y, z=p.z)
    q_conj = quaternion_conjugate(q)
    q_rot = quaternion_mult(quaternion_mult(q,r), q_conj)
    return Point(x=q_rot.x, y=q_rot.y, z=q_rot.z)