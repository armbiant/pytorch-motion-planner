import numpy as np
from beamng.scene_config import Scene
from .test_environment import TestEnvironment
import numpy as np
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely.ops import unary_union
from shapely.validation import make_valid
import geopandas as gpd
import matplotlib as plt

class TestEnvironmentBuilder(object):
    @staticmethod
    def make_test_environment():
        goal_point = np.array([2.5, 2.5], dtype=np.float32)
        start_point = np.array([0.5, 0.5], dtype=np.float32)
        trajectory_boundaries = np.array([-0.1, 3.1, -0.1, 3.1])
        return TestEnvironment(start_point, goal_point, trajectory_boundaries,
                               TestEnvironmentBuilder._obstacle_points())

    @staticmethod
    def _obstacle_points():
        collision_points1_y = np.linspace(0, 2, 10)
        collision_points1 = np.stack([np.ones(10) * 1.15, collision_points1_y], axis=1)
        collision_points2 = collision_points1.copy()
        collision_points2[:, 0] = 1.85
        collision_points2[:, 1] += 1
        return np.concatenate([collision_points1, collision_points2], axis=0)

    @staticmethod
    def _point_line(start, end, point_count):
        x = np.linspace(start[0], end[0], point_count)
        y = np.linspace(start[1], end[1], point_count)
        return np.stack([x, y], axis=1)

    @staticmethod
    def make_test_environment_with_angles():
        goal_point = np.array([2.5, 1.5, 0], dtype=np.float32)
        start_point = np.array([0.5, 0.5, 0], dtype=np.float32)
        trajectory_boundaries = np.array([-0.1, 3.1, -0.1, 3.1])
        return TestEnvironment(start_point, goal_point, trajectory_boundaries,
                               TestEnvironmentBuilder._obstacle_points())

    def make_car_environment(self):
        goal_point = np.array([2, 2.7, 0], dtype=np.float32)
        start_point = np.array([0.5, 1.5, 0], dtype=np.float32)
        trajectory_boundaries = np.array([-0.1, 3.1, -0.1, 3.1])
        return TestEnvironment(start_point, goal_point, trajectory_boundaries,
                               TestEnvironmentBuilder._car_obstacle_points())

    def make_highway_environment(self, car_obstacle_length=4.7,
                                car_obstacle_width=2.1,
                                car_center_x=0, car_center_y=17,
                                start_point = np.array([0, 1, 0], dtype=np.float32),
                                goal_point = np.array([0, 34, 0], dtype=np.float32),
                                trajectory_boundaries = np.array([-20, 20, -1, 40])):# -x +x -y +y

        return TestEnvironment(start_point, goal_point, trajectory_boundaries,
                               TestEnvironmentBuilder.beamng_car_obstacle_points(car_obstacle_length,car_obstacle_width,car_center_x,car_center_y))
    
    def make_environment_from_roads(self, roads: np.ndarray):       
        j = np.sum([len(road) for road in roads])
        points = np.zeros((int(2*j), 2), dtype=np.float32)
        k = 0
        for i in range(len(roads)):
            for j in range(len(roads[i])):
                points[k][0] = roads[i][j]["left"][0]
                points[k][1] = roads[i][j]["left"][1]
                points[k+1][0] = roads[i][j]["right"][0]
                points[k+1][1] = roads[i][j]["right"][1]
                k += 2
        return TestEnvironment(Scene.cross.start_point,
                                Scene.cross.goal_point,
                                Scene.cross.trajectory_boundaries,
                                points)

    def make_environment_gpd_polygon(self, roads: np.ndarray, bounds : list):       
        j = 0
        for road in roads:
            j += len(road)
        points = np.zeros((int(2*j), 2), dtype=np.float32)
        k = 0
        polys = []
        for i in range(len(roads)):
            poly = []
            tmp = k
            for j in range(len(roads[i])):
                x = roads[i][j]["right"][0]
                y = roads[i][j]["right"][1]
                points[k][0] = np.around(x, decimals=1)
                points[k][1] = np.around(y,decimals=1)
                poly.append([points[k][0], points[k][1]])
                k += 1
            tmp = []
            for j in range(len(roads[i])):
                x = roads[i][j]["left"][0]
                y = roads[i][j]["left"][1]
                points[k][0] = np.around(x,decimals=1)
                points[k][1] = np.around(y,decimals=1)
                tmp.append([points[k][0], points[k][1]])
                k += 1
            poly += tmp[::-1]
            polys.append(make_valid(Polygon(poly)))
        polygons = gpd.GeoSeries(unary_union(polys))
        polygons = polygons.clip_by_rect(xmin=bounds[0], ymin=bounds[2], xmax=bounds[1], ymax=bounds[3])
        return TestEnvironment(Scene.cross.start_point,
                                Scene.cross.goal_point,
                                Scene.cross.trajectory_boundaries,
                                polygons)

    def make_environment_from_waypoints(self, waypoints: np.ndarray):       
        return TestEnvironment(Scene.cross.start_point,
                                Scene.cross.goal_point,
                                Scene.cross.trajectory_boundaries,
                                waypoints)

    def make_cross_environment(self):
        return TestEnvironment(Scene.cross.start_point,
         Scene.cross.goal_point,
         Scene.cross.trajectory_boundaries,
         np.concatenate([TestEnvironmentBuilder.beamng_car_obstacle_points(),
                            TestEnvironmentBuilder.static_form_builder(Scene.cross.static_obstacle_points)]))
    
    @staticmethod
    def beamng_car_obstacle_points(length=4.7, width=2.1, cx=0, cy=17, rot=np.eye(3,3)):
        p1 = np.add([cx, cy, 0.0], np.matmul(rot, np.array([width/2, length/2, 0.0])))
        p2 = np.add([cx, cy, 0.0], np.matmul(rot, np.array([width/2, -length/2, 0.0])))
        p3 = np.add([cx, cy, 0.0], np.matmul(rot, np.array([-width/2, -length/2, 0.0])))
        p4 = np.add([cx, cy, 0.0], np.matmul(rot, np.array([-width/2, length/2, 0.0])))
        return np.concatenate([
            TestEnvironmentBuilder._point_line((p1[0], p1[1]), (p2[0], p2[1]), 10),
            TestEnvironmentBuilder._point_line((p2[0], p2[1]), (p3[0], p3[1]), 10),
            TestEnvironmentBuilder._point_line((p3[0], p3[1]), (p4[0], p4[1]), 10),
            TestEnvironmentBuilder._point_line((p4[0], p4[1]), (p1[0], p1[1]), 10)
        ])
    
    @staticmethod
    def _car_obstacle_points():
        y1 = 2.3
        x1 = 1.6
        return np.concatenate([
            TestEnvironmentBuilder._point_line((0, y1), (x1, y1), 10),
            TestEnvironmentBuilder._point_line((x1, y1), (x1, 3), 10),
            TestEnvironmentBuilder._point_line((2.5, y1), (2.5, 3), 10),
            TestEnvironmentBuilder._point_line((2.5, y1), (3, y1), 10),
        ])
    
    @staticmethod
    def static_form_builder(points:np.ndarray, rot=np.eye(3,3)):
        assert points.size != 0        
        assert len(points.shape) == 2
        assert points.shape[0] > 1
        assert points.shape[1] == 2
        num_points = len(points)

        # points should be in relative CS [x,y] to ego vehicle
        for i in range(num_points):
            points[i] = np.matmul(rot, np.array([points[i][0], points[i][1], 0.0]))
        
        obstacle = None

        for i in range(num_points):
            p1 = points[i % num_points]
            p2 = points[(i+1) % num_points]
            if obstacle is None:
                obstacle = TestEnvironmentBuilder._point_line((p1[0], p1[1]), (p2[0], p2[1]), 10)
            else:
                obstacle = np.concatenate([obstacle, TestEnvironmentBuilder._point_line((p1[0], p1[1]), (p2[0], p2[1]), 10)])
        return obstacle