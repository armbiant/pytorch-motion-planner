import numpy as np
import torch.nn.functional
from matplotlib import pyplot as plt
from matplotlib.collections import QuadMesh
from threading import Lock
from copy import deepcopy
from .onf_model import ONF

class Plotter():
    def __init__(self, model:ONF, boundaries:tuple, obstacles:np.ndarray ,trajectory:np.ndarray):
        self.fig = plt.figure(dpi=150)
        self.fig.add_axes([0.02,0.02,0.96,0.96])
        self.fig.axes[0].set_aspect("equal")
        self.fig.axes[0].axis('off')
        self.fig.axes[0].set_xlim(boundaries[0], boundaries[1])
        self.fig.axes[0].set_ylim(boundaries[2], boundaries[3])
        
        self.model = deepcopy(model)
        self.trajectory_points = deepcopy(trajectory)
        self.obstacle_points = deepcopy(obstacles)

        self.heatmap:QuadMesh = None
        self.obstacles = None
        self.trajectory = None
        
        self.model_mutex = Lock()
        self.obstacle_mutex = Lock()
        self.trajectory_mutex = Lock()
        self.gridsize = 100
        grid_x, grid_y = np.meshgrid(np.linspace(boundaries[0], boundaries[1], self.gridsize),
                                    np.linspace(boundaries[2], boundaries[3], self.gridsize))
        self.grid = np.stack([grid_x, grid_y, np.zeros_like(grid_x)], axis=2).reshape(-1, 3)
        self.mesh = self.grid.reshape(self.gridsize,self.gridsize,3)
        self.grid_tensor = torch.tensor(self.grid.astype(np.float32), device="cpu")
        

    def drawnow(self):
        self.fig.show()
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()

    def update_objects(self):
        self.plot_model_heatmap()
        self.plot_obstacles()
        self.plot_trajectory()

    def update_plotter_data(self, trajectory=None, model=None, obstacles=None):
        if trajectory is not None:
            self._update_trajectory(trajectory)
        if obstacles is not None:
            self._update_obstacles(obstacles)
        if model is not None:
            self._update_model(model)

    def _update_trajectory(self, points):
        self.trajectory_mutex.acquire()
        self.trajectory_points = deepcopy(points)
        self.trajectory_mutex.release()

    def _update_obstacles(self, points):
        self.obstacle_mutex.acquire()
        self.obstacle_points = deepcopy(points)
        self.obstacle_mutex.release()

    def _update_model(self, model):
        self.model_mutex.acquire()
        self.model = deepcopy(model)
        self.model_mutex.release()

    def get_heatmap(self) -> QuadMesh:
        if self.heatmap is not None:
            return self.heatmap
    
    def plot_model_heatmap(self, device="cpu"):
        self.model_mutex.acquire()
        model = deepcopy(self.model)
        self.model_mutex.release()

        obstacle_probabilities = model(torch.tensor(self.grid.astype(np.float32), device=device))
        obstacle_probabilities = obstacle_probabilities.cpu().detach().numpy().reshape(self.gridsize, self.gridsize)
        if (self.heatmap is None):
            self.heatmap = self.fig.axes[0].pcolormesh(self.mesh[:, :, 0], self.mesh[:, :, 1], obstacle_probabilities, cmap='coolwarm', shading='auto')
        else:
            self.heatmap.set_array(obstacle_probabilities)

    def plot_trajectory(self):
        self.obstacle_mutex.acquire()
        points = deepcopy(self.trajectory_points)
        self.obstacle_mutex.release()

        if (points is not None):
            if (self.trajectory is None):
                self.trajectory = self.fig.axes[0].scatter(points[:, 0], points[:, 1], color="yellow", s=10)
            else:
                self.trajectory.set_offsets(points)
    
    def plot_obstacles(self):
        self.obstacle_mutex.acquire()
        points = deepcopy(self.obstacle_points)
        self.obstacle_mutex.release()

        if (points is not None):
            if (self.obstacles is None):
                self.obstacles = self.fig.axes[0].scatter(points[:, 0], points[:, 1], color="black")
            else:
                self.obstacles.set_offsets(points)