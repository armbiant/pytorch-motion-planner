from dataclasses import dataclass
from pytorch_lightning.utilities import AttributeDict
import numpy as np
import os


@dataclass
class Scene():
    cross = AttributeDict(
        ego_pose=AttributeDict(
            pos=[-567.24, 720.19, 81.63],
            rotation=[0.034, 0.072, 0.934, -0.346]
        ),
        dummy_pose=AttributeDict(
            pos=[-684.42,661.72,96.49],
            rotation=[0.053, -0.05, 0.71, 0.69]
        ),
        dummy_line_script = np.load("C:/Users/user/Documents/pytorch-motion-planner/beamng/dummy_script_21_02_23.npy", allow_pickle=True),
        goal_point=np.array([-653.79, 482.21, 0.7], dtype=np.float32,),
        start_point=np.array([-567.24, 720.19, 0.7], dtype=np.float32),
        trajectory_boundaries=np.array([-745, -435, 440, 750], dtype=np.float32),
        # trajectory_boundaries=np.array([-1000, 1000, -1000, 1000], dtype=np.float32),
        global_start_point=np.array([-547.54, 770.16, 74.87]),
        global_goal_point=np.array([-558.4, 594.9, 90.2], dtype=np.float32)
    )
    highway = AttributeDict(
        ego_pose=AttributeDict(
            pos=[354.00, 649.00, 116.00],
            rotation=[0., 0., 0.38, 0.92]
        ),
        dummy_pose=AttributeDict(
            pos=[354.00, 644.00, 116.00],
            rotation=[0., 0., 0.38, 0.92]
        )
    )


DEFAULT_PARAMS = AttributeDict(
    device="cpu",
    trajectory_length=80,
    collision_model=AttributeDict(
        mean=0,
        sigma=15,
        use_cos=True,
        bias=True,
        use_normal_init=True,
        angle_encoding=True,
        name="ONF"
    ),
    trajectory_initializer=AttributeDict(
        name="AstarTrajectoryInitializer",
        resolution=10
    ),
    collision_optimizer=AttributeDict(
        lr=1e-1,
        betas=(0.9, 0.9)
    ),
    trajectory_optimizer=AttributeDict(
        lr=2e-1,
        betas=(0.8, 0.8)
    ),
    planner=AttributeDict(
        name="ConstrainedNERFOptPlanner",
        velocity_hessian_weight         = 5e1,
        angle_weight                    = 5e2,
        collision_weight                = 5e3,
        constraint_deltas_weight        = 5e2,
        boundary_weight                 = 1e-1,
        distance_loss_weight            = 1e-1,

        trajectory_random_offset        = 3,
        angle_offset                    = 0.3,
        random_field_points             = 10,
        init_collision_iteration        = 5,
        init_collision_points           = 100,
        reparametrize_trajectory_freq   = 10,
        optimize_collision_model_freq   = 1,

        multipliers_lr                  = 2e-1,
        collision_multipliers_lr        = 1e-2
    )
)