from scene_config import Scene, DEFAULT_PARAMS, TestConfig
from neural_field_optimal_planner import plotter, onf_model
from planner_utils import *
from dataclasses import dataclass
from neural_field_optimal_planner.test_environment_builder import TestEnvironmentBuilder
from neural_field_optimal_planner.plotting_utils import *
from neural_field_optimal_planner.planner_factory import PlannerFactory
from neural_field_optimal_planner.collision_checker import CarCollisionChecker
import rclpy
from rclpy.node import Node
from rclpy.executors import MultiThreadedExecutor, SingleThreadedExecutor
from beamngpy.logging import BNGError
from beamngpy import quat
from matplotlib.collections import QuadMesh

from geometry_msgs.msg import Pose, Point, Quaternion, Transform, Vector3
from std_msgs.msg import Float32MultiArray, MultiArrayDimension, MultiArrayLayout
from multiprocessing import Process, Queue, shared_memory

import numpy as np
import torch
import time
from pytorch_lightning.utilities import AttributeDict

from sim_interface import SimInterface as SimInterface
import sys
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/astar')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/utils')


torch.random.manual_seed(100)
np.random.seed(400)


class Planner():
    def __init__(self, vehicle_names, sim):

        self._trajectory_points = None
        self._sim = sim
        self._ego_length = 2.1  # x
        self._ego_width = 4  # y

        self._planner = None
        self._planner_parameters = None
        self._test_environment = None
        self._collision_checker = None
        self._dummy_bbox = [4.7, 2.1, 0.0]

        self.planner_vehicles_poses = dict()
        self.vehicle_names = vehicle_names


class PlannerNode(Node):
    def __init__(self, sim: SimInterface):
        super().__init__('planner_node')

        self.vehicle_names = sim.getCurrentCarNames()

        self.planner_vehicle_subs = dict()
        for vehicle_name in self.vehicle_names:
            self.planner_vehicle_subs[vehicle_name] = self.create_subscription(
                Pose, (vehicle_name+'_pose'), self.make_callback(vehicle_name), 10)

        self.pub_plot_traj = self.create_publisher(
            Float32MultiArray, 'plot_trajectory', 10)

        # self._previous_env_rotation = None

        self.start_checker_timer = self.create_timer(1, self.check_for_start)
        self.publish_trajectory_timer = self.create_timer(
            0.02, self.publish_trajectory)

        self.processes: list[Process] = []

        self.shm_planner_model = shared_memory.SharedMemory(
            create=True, size=sys.getsizeof(onf_model.ONF))
        self.shm_vehicle_poses = []
        for pose in self.shm_vehicle_poses:
            self.shm_vehicle_poses.append(shared_memory.SharedMemory(
                create=True, size=sys.getsizeof(Pose)))

        self.log = TestConfig.log
        self.ready = False
        self.heatmap_delay = True
        self.heatmap_ids = []

        print("Planner node initialized") if self.log else None

    def check_for_start(self):
        # TODO переписать на флаг
        if (self.planner_vehicles_poses["ego"] is not None):
            print("Preparing planner")
            self.prepare_planner()
            self.destroy_timer(self.start_checker_timer)
            self.ready = True
        else:
            print("Waiting for sim_node to publish vehicle states...")

    def make_callback(self, vehicle):
        def sub_get_pose(self, msg):
            self.planner_vehicle_queues[vehicle].put(msg)
        setattr(self, 'sub_get_pose', sub_get_pose.__get__(
            self, self.__class__))  # bind
        return self.sub_get_pose

    def calculate_dummy_rel_pos(self) -> Point:
        tf_ego = Transform(translation=point_to_vector3(self.planner_vehicles_poses["ego"].position),
                           rotation=self.planner_vehicles_poses["ego"].orientation)
        tf_dummy = Transform(translation=point_to_vector3(self.planner_vehicles_poses["dummy"].position),
                             rotation=self.planner_vehicles_poses["dummy"].orientation)
        rel_transform = Vector3(x=tf_dummy.translation.x - tf_ego.translation.x,
                                y=tf_dummy.translation.y - tf_ego.translation.y,
                                z=tf_dummy.translation.z - tf_ego.translation.z)
        rel_transform = rotate_point_by_quaternion(
            p=rel_transform, q=tf_ego.rotation)
        return Point(x=rel_transform.x, y=rel_transform.y, z=rel_transform.z)

    def calculate_target_rel_pos(self) -> Point:
        tf_ego = Transform(translation=point_to_vector3(self.planner_vehicles_poses["ego"].position),
                           rotation=self.planner_vehicles_poses["ego"].orientation)
        tf_dummy = Transform(translation=point_to_vector3(self.planner_vehicles_poses["dummy"].position),
                             rotation=self.planner_vehicles_poses["dummy"].orientation)
        p1 = rotate_point_by_quaternion(p=Point(x=TestConfig.rel_target_dx,
                                                y=TestConfig.rel_target_dy,
                                                z=TestConfig.rel_target_dz),
                                        q=quaternion_conjugate(tf_dummy.rotation))

        p2 = Vector3(x=tf_dummy.translation.x + p1.x,
                     y=tf_dummy.translation.y + p1.y,
                     z=tf_dummy.translation.z + p1.z)
        tf_target = Transform(
            translation=p2, rotation=self.planner_vehicles_poses["dummy"].orientation)
        rel_transform = Vector3(x=tf_target.translation.x - tf_ego.translation.x,
                                y=tf_target.translation.y - tf_ego.translation.y,
                                z=tf_target.translation.z - tf_ego.translation.z)
        rel_transform = rotate_point_by_quaternion(
            p=rel_transform, q=self.planner_vehicles_poses["ego"].orientation)
        return Point(x=rel_transform.x, y=rel_transform.y, z=rel_transform.z)

    def planner_trajectory_to_global(self, trajectory: np.ndarray) -> np.ndarray:
        pos = self.planner_vehicles_poses["ego"].position
        quat = self.planner_vehicles_poses["ego"].orientation
        rotated = np.insert(trajectory, 0, [0.0, 0.0, 0.0], axis=0)
        for i in range(len(rotated)):
            x = rotated[i][0].item()
            y = rotated[i][1].item()
            z = 0.0
            p = rotate_point_by_quaternion(p=point_to_vector3(
                Point(x=x, y=y, z=z)), q=quaternion_conjugate(quat))
            rotated[i] = np.add(np.array([p.x, p.y, 119.0]),
                                np.array([pos.x, pos.y, 0.0]))
        return rotated

    def plot_heatmap_spheres(self, plt: plotter.Plotter):
        heatmap = plt.get_heatmap()
        colors = heatmap.cmap(heatmap.norm(heatmap.get_array()))
        spheres = self.get_points_from_heatmap(heatmap)
        d = 0.0

        self.heatmap_ids.append(self._sim.plot_debug_heatmap_spheres(
            sphere_coord=spheres, color_arr=colors, offset=0.1+d))

        if (self.heatmap_delay):
            self.heatmap_delay = not self.heatmap_delay
        else:
            self._sim.remove_debug_heatmap_spheres(self.heatmap_ids[0])
            self.heatmap_ids[0] = self.heatmap_ids[1]
            del self.heatmap_ids[1]
        d += 0.001
        if (d > 0.1):
            d = 0

    def get_points_from_heatmap(self, heatmap: QuadMesh):
        pos = self.planner_vehicles_poses["ego"].position
        quat = self.planner_vehicles_poses["ego"].orientation
        coords = heatmap.get_coordinates()
        rows = coords.shape[0]
        cols = coords.shape[1]

        points = np.zeros(((rows-1)*(cols-1), 3))
        k = 0
        for i in range(rows-1):
            for j in range(cols-1):
                x = coords[i][j][0].item()
                y = coords[i][j][1].item()
                z = 0.0
                p = rotate_point_by_quaternion(p=point_to_vector3(
                    Point(x=x, y=y, z=z)), q=quaternion_conjugate(quat))
                points[k] = np.add(np.array([p.x, p.y, 119.0]),
                                   np.array([pos.x, pos.y, 0.0]))
                k += 1

        return points

    def plot_heatmap_vertices(self, plt: plotter.Plotter):
        heatmap = plt.get_heatmap()
        colors = heatmap.cmap(heatmap.norm(heatmap.get_array()))
        rectangles = self.get_vertices_from_heatmap(heatmap)
        if (len(colors) * len(colors[0]) == len(rectangles)):
            self._sim.plot_debug_rectangles(
                rect_coord=rectangles, color_arr=colors)

    def get_vertices_from_heatmap(self, heatmap: QuadMesh):
        pos = self.planner_vehicles_poses["ego"].position
        quat = self.planner_vehicles_poses["ego"].orientation
        coords = heatmap.get_coordinates()
        rows = coords.shape[0]
        cols = coords.shape[1]

        rotated = np.zeros((rows, cols, 3))
        rectangles = np.zeros(((rows-1)*(cols-1), 4, 3))
        k = 0
        for i in range(rows):
            for j in range(cols):
                x = coords[i][j][0].item()
                y = coords[i][j][1].item()
                z = 0.0
                p = rotate_point_by_quaternion(p=point_to_vector3(
                    Point(x=x, y=y, z=z)), q=quaternion_conjugate(quat))
                rotated[i][j] = np.add(
                    np.array([p.x, p.y, 119.0]), np.array([pos.x, pos.y, 0.0]))

        for i in range(rows-1):
            for j in range(cols-1):
                rectangles[k] = [rotated[i][j], rotated[i+1]
                                 [j], rotated[i][j+1], rotated[i+1][j+1]]
                k += 1
        return rectangles

    def prepare_planner(self, planner_params: AttributeDict = DEFAULT_PARAMS):
        self._planner_parameters = planner_params
        dummy_rel_pos = self.calculate_dummy_rel_pos()
        self._test_environment = \
            TestEnvironmentBuilder().make_highway_environment(car_obstacle_length=self._dummy_bbox[1],
                                                              car_obstacle_width=self._dummy_bbox[0],
                                                              car_center_x=dummy_rel_pos.x,
                                                              car_center_y=dummy_rel_pos.y,
                                                              start_point=np.array(
                                                                  [0, 2, 0], dtype=np.float32),
                                                              goal_point=np.array(
                                                                  [10, 38, 0], dtype=np.float32,),
                                                              trajectory_boundaries=np.array([-15, 15, -1,  40]))

        self._collision_checker = CarCollisionChecker(box=(-self._ego_length/2, self._ego_length/2, -self._ego_width/2, self._ego_width/2),
                                                      collision_boundaries=[-15, 15, 0, 40])
        self._collision_checker.update_obstacle_points(
            self._test_environment.obstaclePoints)
        self._planner = PlannerFactory.make_constrained_onf_planner(
            self._collision_checker, self._planner_parameters)

        goal_point = self._test_environment.goalPoint
        start_point = self._test_environment.startPoint
        trajectory_boundaries = self._test_environment.bounds
        self._planner.init(start_point, goal_point, trajectory_boundaries)

    def update_planner_environment(self):
        dummy_rel_pos = self.calculate_dummy_rel_pos()
        q1 = self.planner_vehicles_poses["ego"].orientation
        q2 = self.planner_vehicles_poses["dummy"].orientation

        # if self._previous_env_rotation is None:
        #     self._previous_env_rotation = q1
        #     env_rotation = self._previous_env_rotation
        # else:
        #     env_rotation = quaternion_mult(quaternion_conjugate(self._previous_env_rotation), q1)
        #     self._previous_env_rotation = q1

        q = quaternion_mult(quaternion_conjugate(q2), q1)
        rm = quat.compute_rotation_matrix([q.x, q.y, q.z, q.w])
        obstacle_points = TestEnvironmentBuilder.beamng_car_obstacle_points(length=self._dummy_bbox[0], width=self._dummy_bbox[1],
                                                                            cx=dummy_rel_pos.x, cy=dummy_rel_pos.y, rot=rm)

        # vector3s = [rotate_point_by_quaternion(p=Vector3(x=point[0], y=point[1], z=0.), q=env_rotation) \
        #                 for point in self._test_environment.obstacle_points]
        # obstacle_points = np.asarray([[point.x, point.y] for point in vector3s])

        start_point = np.array([0.0, 0.0, 0.0])
        target_rel_pos = self.calculate_target_rel_pos()
        goal_point = np.array([target_rel_pos.x, target_rel_pos.y, 0.0])

        self._collision_checker.update_obstacle_points(obstacle_points)
        self._planner.update_start_point(start_point)
        # self._planner.update_goal_point(goal_point)

        self._test_environment.goalPoint = goal_point
        self._test_environment.startPoint = start_point
        self._test_environment.obstaclePoints = obstacle_points

    def get_new_trajectory(self) -> Float32MultiArray:
        trajectory = self._planner.get_full_trajectory()
        rotated = self.planner_trajectory_to_global(trajectory)
        row = MultiArrayDimension(label="row", size=len(
            rotated), stride=len(rotated)*len(rotated[0]))
        col = MultiArrayDimension(label="col", size=len(
            rotated[0]), stride=len(rotated[0]))
        lay = MultiArrayLayout(dim=[row, col], data_offset=0)
        return Float32MultiArray(layout=lay, data=[item for sublist in rotated for item in sublist])

    def publish_trajectory(self):
        trajectory = self.pub_trajectory_queue.get()
        self.pub_plot_traj.publish(trajectory)

    def planner_process(self, log=TestConfig.log):
        print("Waiting for ros to start spinning ... ")

        while (rclpy.ok()):
            for vehicle_name in self.vehicle_names:
                self.planner_vehicles_poses[vehicle_name] = self.planner_vehicle_pose_queues[vehicle_name].get(
                )
            start = self.get_clock().now()

            self.update_planner_environment()
            self._planner.step()
            self.model_queue.put(self._planner._collision_model)

            traj = self._planner.get_full_trajectory()
            self.plot_trajectory_queue.put(traj)

            msg: Float32MultiArray = self.get_new_trajectory()
            self.pub_trajectory_queue.put(msg)

            # print(f"Planner step took: {(self.get_clock().now() - start).nanoseconds/1e9} seconds")
        print("Planner process done.")

    def plotter_process(self):
        model = self.model_queue.get()
        trajectory = self.plot_trajectory_queue.get()

        plt = plotter.Plotter(model=model, boundaries=self._test_environment.bounds,
                              obstacles=self._test_environment.obstaclePoints, trajectory=trajectory)
        while (rclpy.ok()):

            for vehicle_name in self.vehicle_names:
                self.plotter_vehicles_poses[vehicle_name] = self.plotter_vehicle_pose_queues[vehicle_name].get(
                )

            tr = self.plot_trajectory_queue.get()
            model = self.model_queue.get()
            plt.update_plotter_data(model=self._planner._collision_model,
                                    trajectory=tr, obstacles=self._test_environment.obstaclePoints)
            plt.plot_model_heatmap()
            # Matplotlib 2D plot
            plt.update_objects()
            plt.drawnow()

            # In-game vertices spawning (too slow)
            # self.plot_heatmap_vertices(plt)

            # In-game sphere spawning
            # self.plot_heatmap_spheres(plt)


def main():
    rclpy.init(args=None)
    sim = SimInterface.create_sim_interface_instance()
    if sim is None:
        sys.exit(-1)

    node = PlannerNode(sim=sim)
    print("Created sim instance")

    planner = Process(target=node.planner_process, args=(), daemon=False)
    plotter = Process(target=node.plotter_process, args=(), daemon=False)
    planner.start()
    plotter.start()

    while (rclpy.ok()):
        rclpy.spin_once(node)

    print("Started planner")
    for process in node.processes:
        process.join()
    node.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
