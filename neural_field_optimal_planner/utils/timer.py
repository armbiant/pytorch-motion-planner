import time
from collections import defaultdict

import numpy as np


class Timer(object):
    def __init__(self):
        """Simple timer for benchmarking some code.
            - `timer.tick` is a start of timer counter. Start time is the time in seconds from
                the beggining of the epoch. May be called once again to start a new segment.
            - `timer.tock` is an end of timer counter segment. It can be called multiple times,
                each segment is keeped separately and later is used to calculate 
                metrics of benchmark.
        """
        self._starts = defaultdict(float)
        self._deltas = defaultdict(list)
        self._counts = defaultdict(int)

    def tick(self, name=None):
        """Start a new timer
            May be called once again to start a new segment.
        Args:
            name (_type_, optional): Name of the new timer. 
                Use this name to call the `timer.tock()` later in code.
        """
        self._starts[name] = time.time()
        self._counts[name] += 1

    def tock(self, name=None):
        """Stop the timer
            May be called several times to catch the delta time between start and end of
                this segment.
        Args:
            name (_type_, optional): Name of the timer which was started by `timer.tick()`
        """
        self._deltas[name].append(time.time() - self._starts[name])

    def print(self):
        for key, value in self._deltas.items():
            mean = np.round(1000 * np.mean(value), 1)
            std = np.round(1000 * np.std(value), 1)
            print("Duration of", key, "=", mean, "+-", std, "ms")


timer = Timer()