import sys
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/astar')
sys.path.append(
    'C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner/utils')

from geometry_msgs.msg import Pose, Point, Quaternion, Twist, Vector3
from sim_interface import SimInterface
import numpy as np
from multiprocessing import Queue, Event
import queue
import string
import time
class Sim():
    def __init__(self, sim: SimInterface, qCarPoses: dict, qTraj: Queue, qScripts: dict, run_flag: Event):
        self.log = False
        self.sim = sim
        self.carNames = sim.getCurrentCarNames()
        self.run = run_flag
        self.qCarPoses = qCarPoses
        self.qTraj = qTraj
        self.qScripts = qScripts
        self.scripts = {
            "ego": None,
            "dummy" : None
        }

        self.carPoses = dict()
        self.updateVehiclePoses()
        for car in self.qCarPoses:
            self.qCarPoses[car].put(self.carPoses[car])
        try:
            self.traj = qTraj.get(block=False)
        except queue.Empty:
            self.traj = None
        self.sphere_ids = []
        print("[Sim] Initialized")

    def process(self):
        while (not self.run.is_set()):
            self.updateVehiclePoses()
            self.sendTrajToSim()
            self.sendScriptsToSim()
            for car in self.carNames:
                try:
                    self.qCarPoses[car].put(self.carPoses[car], block=False)
                except queue.Full:
                    pass
            try:
                self.traj = self.qTraj.get(block=False)
            except queue.Empty:
                pass

            try:
                self.scripts["dummy"] = self.qScripts["dummy"].get(block=False)
                self.scripts["ego"] = self.qScripts["ego"].get(block=False)
            except queue.Empty:
                pass

            print("[Sim] Running") if self.log else None
        print("[Sim] Cleanup")
        self.sim.remove_all_debug_graphics()
        print("[Sim] Shutdown")

    def sendScriptsToSim(self):
        if (self.scripts["ego"] is not None):
            self.sim.sendScript("ego", self.scripts["ego"])
            self.scripts["ego"] = None
        if (self.scripts["dummy"] is not None):
            self.sim.sendScript("dummy", self.scripts["dummy"])
            self.scripts["dummy"] = None
        

    def updateVehiclePoses(self) -> dict:
        for car in self.carNames:
            p, q = self.sim.get_vehicle_pose(self.sim.vehicles[car])
            # v = self.sim.get_vehicle_velocity(self.sim.vehicles[vehicle])
            pose = Pose(position=Point(x=p[0], y=p[1], z=p[2]),
                        orientation=Quaternion(x=q[0], y=q[1], z=q[2], w=q[3]))
            self.carPoses[car] = pose

    def sendTrajToSim(self):
        if (self.traj is not None):
            points = self.traj
            for i in range(47):
                points[i][2] = 92
            for i in range(47,82):
                points[i][2] = 400    
            self.sim.plot_debug_polylines(points)
            self.sphere_ids.append(self.sim.plot_debug_spheres(self.traj))
            if (len(self.sphere_ids) == 2):
                self.sim.remove_debug_spheres(self.sphere_ids[0])
                del self.sphere_ids[0]
            self.sim.remove_debug_polylines(self.sim.debug_poly_ids)
            