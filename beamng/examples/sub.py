import rclpy
from rclpy.node import Node
import sys
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner')
sys.path.append('C:/Users/user/Documents/pytorch-motion-planner/neural_field_optimal_planner')

import rclpy
from rclpy.node import Node
from rclpy.executors import MultiThreadedExecutor
from std_msgs.msg import String
from threading import Thread
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import String


class MinimalSubscriber(Node):

    def __init__(self):
        super().__init__('minimal_subscriber')
        self.subscription = self.create_subscription(
            String,
            'topic',
            self.listener_callback,
            10)
        self.subscription  # prevent unused variable warning
        self._sub_plot_poly = self.create_subscription(Float32MultiArray, 'plot_trajectory',
                                                     self.sub_plot_trajectory, 10)
    def listener_callback(self, msg):
        self.get_logger().info('I heard: "%s"' % msg.data)
    
    def sub_plot_trajectory(self, msg):
        self.get_logger().info('I heard: "%s"' % msg)


def main(args=None):
    rclpy.init(args=args)
    node = MinimalSubscriber()
    executor = MultiThreadedExecutor()
    executor.add_node(node)
    executor_thread = Thread(target=executor.spin, daemon=False)
    executor_thread.start()
    input()
    
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()